accessing
baseline: spec 
	<baseline>
	
	spec for: #pharo do: [
		spec repository: 'filetree://repository/mc'. 
		
		self magritte3: spec.
		self voyage: spec.
		self petitParser: spec.
		self seaside3: spec.
		self reef: spec.
		
		spec
			package: 'EVE-Tools-Core' with: [
				spec requires: #('Magritte3' 'Voyage' 'PetitParser') ];
			package: 'EVE-Tools-View' with: [
				spec requires: #('EVE-Tools-Core' 'Magritte-Seaside') ];
			package: 'EVE-Tools-Spy' with: [
				spec requires: #('EVE-Tools-Core' 'Magritte-Seaside' 'Reef' 'Magritte-Reef' 'EVE-Tools-View') ];
			package: 'EVE-Tools-CRP' with: [ 
				spec requires: #('EVE-Tools-Core' 'Magritte-Seaside' 'Reef' 'Magritte-Reef' 'EVE-Tools-View') ];
			package: 'EVE-Tools-Tests' with: [  
				spec requires: #('EVE-Tools-Core') ].
			
		spec
			group: 'default' with: #('core' 'spy' 'crp' 'tests');
			group: 'core' with: #('EVE-Tools-Core');
			group: 'spy' with: #('Seaside3' 'EVE-Tools-View' 'EVE-Tools-Spy');
			group: 'crp' with: #('Seaside3' 'EVE-Tools-View' 'EVE-Tools-CRP');
			group: 'tests' with: #('EVE-Tools-Tests') ].
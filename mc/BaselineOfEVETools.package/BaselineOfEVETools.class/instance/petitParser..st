accessing
petitParser: spec
	spec repository: 'http://smalltalkhub.com/mc/Moose/PetitParser/main'.
	
	spec 
		project: 'PetitParser-Base' with: [ 
			spec
				className: 'ConfigurationOfPetitParser';
				versionString: #stable;
				loads: #('Core');
				repository: 'http://smalltalkhub.com/mc/Moose/PetitParser/main' ];
		package: 'PetitXml' with: [ 
			spec requires: #('PetitParser-Base') ];
		package: 'PetitXPath' with: [ 
			spec requires: #('PetitXml') ].	
			
		spec group: 'PetitParser' with: #('PetitParser-Base' 'PetitXml' 'PetitXPath')
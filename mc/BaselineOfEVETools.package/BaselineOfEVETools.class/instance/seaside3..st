accessing
seaside3: spec
	spec project: 'Seaside3' with: [ 
		spec
			className: 'ConfigurationOfSeaside3';
			versionString: #stable;
			loads: #('default' 'JSON' 'REST');
			repository: 'http://smalltalkhub.com/mc/Seaside/MetacelloConfigurations/main' ].
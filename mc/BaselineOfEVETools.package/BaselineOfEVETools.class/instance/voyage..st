accessing
voyage: spec
	spec repository: 'http://smalltalkhub.com/mc/estebanlm/Voyage/main'.
	
	spec 
		project: 'Voyage-Base' with: [ 
			spec
				className: 'ConfigurationOfVoyageMongo';
				versionString: #stable;
				repository: 'http://smalltalkhub.com/mc/estebanlm/Voyage/main' ];
		package: 'Voyage-Seaside-Container' with: [ 
			spec requires: #('Voyage-Base') ].
		
	spec group: 'Voyage' with: #('Voyage-Base' 'Voyage-Seaside-Container')	
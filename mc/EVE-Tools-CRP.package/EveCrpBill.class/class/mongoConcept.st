persistence
mongoConcept
	<mongoDescription>
	
	^ VOMongoToOneDescription new
		attributeName: 'concept';
		accessor: (MAPluggableAccessor 
			read: [ :bill | bill concept name ]
			write: [ :bill :value | bill concept: (EveCrpBillConcept findByName: value) ]);
		yourself
persistence
mongoReportingCharacter
	<mongoDescription>
	
	^ VOMongoToOneDescription new 
		attributeName: 'reportingCharacter';
		kind: EveCharacter;
		yourself
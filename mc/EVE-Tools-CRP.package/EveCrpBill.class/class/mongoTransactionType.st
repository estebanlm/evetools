persistence
mongoTransactionType
	<mongoDescription>
	
	^ VOMongoToOneDescription new
		attributeName: 'transactionType';
		accessor: (MAPluggableAccessor 
			read: [ :bill | bill transactionType name ]
			write: [ :bill :value | bill transactionType: (EveCrpTransactionType findByName: value) ]);
		yourself
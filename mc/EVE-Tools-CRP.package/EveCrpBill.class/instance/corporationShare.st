accessing
corporationShare
	^ self concept 
		corporationShareFor: self amount
		rate: self taxRate
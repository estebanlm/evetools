description
descriptionAmount
	<magritteDescription>
	
	^ MANumberDescription new 
		priority: 40;
		label: 'Estimated amount';
		accessor: #amount;
		beRequired;
		yourself 
description
descriptionComment
	<magritteDescription>
	
	^ MAStringDescription new 
		cssClass: 'note';
		priority: 45;
		label: 'Note';
		accessor: #comment;
		yourself 
description
descriptionConcept
	<magritteDescription>
	
	^ MASingleOptionDescription new 
		priority: 30;
		label: 'Concept';
		accessor: #concept;
		default: EveCrpBillConcept loot;
		options: EveCrpBillConcept all;
		beRequired;
		yourself 
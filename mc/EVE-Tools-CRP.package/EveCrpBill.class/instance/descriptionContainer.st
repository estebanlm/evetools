description
descriptionContainer
	"Return the default description container."
	<magritteContainer>
	
	^ super descriptionContainer 
		cssClassForForm: 'form-horizontal';
		yourself
description
descriptionDate
	<magritteDescription>
	
	^ MADateDescription new 
		priority: 10;
		label: 'Date';
		accessor: #date;
		beReadonly;
		yourself 
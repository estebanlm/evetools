description
descriptionFleet
	<magritteDescription>
	
	^ MAMultipleOptionDescription new 
		cssClass: 'fleet';
		priority: 50;
		label: 'Fleet members';
		accessor: #fleet;
		options: EveCorporation unops characters;
		sortBlock: [ :a :b | a plainName < b plainName ];
		reefClass: MAMultiSelectCheckBoxReef;
		columns: 3;
		default: #();
		beRequired;
		yourself 
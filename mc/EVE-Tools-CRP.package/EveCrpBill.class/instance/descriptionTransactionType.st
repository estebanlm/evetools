description
descriptionTransactionType
	"<magritteDescription>"
	"Not needed, I will assume all are debits"
	
	^ MASingleOptionDescription new 
		priority: 20;
		label: 'Type';
		accessor: #transactionType;
		default: EveCrpTransactionType debit;
		options: EveCrpTransactionType all;
		beRequired;
		yourself 
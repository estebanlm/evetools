accessing
fleetAsCommaString
	^ ((self fleet 
		collect: #name)
		sorted: [ :a :b | a < b ])
		asCommaStringAnd
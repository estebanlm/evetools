accessing
shareFor: aCharacter
	^ self sharesByCharacter 
		at: aCharacter 
		ifAbsent: [ 0 ]
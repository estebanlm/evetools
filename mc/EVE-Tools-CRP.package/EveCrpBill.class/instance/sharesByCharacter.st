accessing
sharesByCharacter
	| total alts mains shares size |
	
	shares := Dictionary new.
	total := self amount - self taxes.
	alts := self fleet select: #isAlt.
	mains := self fleet select: #notAlt.
	size := self fleet size.
	
	alts do: [ :each |
		shares 
			at: each 
			put: (self concept altShareFor: (total / size)) asFloat "Alts get half in loot operations" ].
	
	total := total - (shares values
		ifNotEmpty: [ :values | values sum ]
		ifEmpty: [ 0 ]).
	size := size - alts size.
	
	mains do: [ :each |
		shares 
			at: each 
			put: (total / size) asFloat ].
		
	^ shares
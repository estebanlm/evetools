accessing
taxRate
	^ taxRate ifNil: [ taxRate := self class taxRate ]
accessing
taxes
	^ self concept 
		taxesFor: self amount
		rate: self taxRate.
		
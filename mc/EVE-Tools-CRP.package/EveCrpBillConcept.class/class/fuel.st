accessing
fuel
	^ fuel ifNil: [ fuel := self name: 'Fuel' type: EveCrpTransactionType credit taxes: false ]
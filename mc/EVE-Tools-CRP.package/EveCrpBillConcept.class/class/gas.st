accessing
gas
	^ gas ifNil: [ gas := self name: 'Gas' type: EveCrpTransactionType debit ]
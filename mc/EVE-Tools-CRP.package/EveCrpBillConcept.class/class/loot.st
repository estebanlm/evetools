accessing
loot 
	^ loot ifNil: [ 
		loot := self 
			name: 'Loot' 
			type: EveCrpTransactionType debit 
			taxes: true
			altPenalty: true ]
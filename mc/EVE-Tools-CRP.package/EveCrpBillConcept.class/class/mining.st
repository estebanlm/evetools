accessing
mining
	^ mining ifNil: [ mining := self name: 'Mine' type: EveCrpTransactionType debit ]
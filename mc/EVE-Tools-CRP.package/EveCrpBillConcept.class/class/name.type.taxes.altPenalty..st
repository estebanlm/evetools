private
name: aString type: aType taxes: aBoolean altPenalty: altPenaltyBoolean
	^self basicNew
		initializeName: aString 
			type: aType 
			taxes: aBoolean
			altPenalty: altPenaltyBoolean;
		yourself
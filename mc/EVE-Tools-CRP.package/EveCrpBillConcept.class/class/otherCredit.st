accessing
otherCredit
	^ otherCredit ifNil: [ otherCredit := self name: 'Other credit' type: EveCrpTransactionType credit. ]
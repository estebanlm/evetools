accessing
otherDebit
	^ otherDebit ifNil: [ otherDebit := self name: 'Other with tax' type: EveCrpTransactionType debit ]
accessing
pi 
	^ pi ifNil: [ pi := self name: 'PI' type: EveCrpTransactionType debit ]
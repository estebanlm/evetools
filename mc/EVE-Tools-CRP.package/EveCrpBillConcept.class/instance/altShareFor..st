accessing
altShareFor: aNumber
	^ self appliesAltPenalty
		ifTrue: [ aNumber * 0.5 ]
		ifFalse: [ aNumber ]
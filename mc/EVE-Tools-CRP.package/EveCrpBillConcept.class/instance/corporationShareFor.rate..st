accessing
corporationShareFor: aNumber rate: taxNumber
	^ self appliesTaxes 
		ifTrue: [	self taxesFor: aNumber rate: taxNumber ]
		ifFalse: [ aNumber negated ]
accessing
taxesFor: aNumber rate: taxNumber
	^ self appliesTaxes 
		ifTrue: [	aNumber * taxNumber / 100 ]
		ifFalse: [ 0 ]
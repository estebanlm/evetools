factory
createAltBox: aCharacter 
	| checkBox |
	
	checkBox := RECheckBox new 
		value: aCharacter isAlt;
		callback: [ :v | self member: aCharacter alt: v ];
		yourself.
		
	^ REPanel new 
		add: checkBox;
		add: (RELabel new 
			text: aCharacter name; 
			for: checkBox;
			yourself);
		yourself
	
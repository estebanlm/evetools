factory
createAltButtons
	^ REPanel new 
		class: 'buttons';
		add: (REButton new
			class: 'btn';
			label: 'Accept';
			callback: [] asReefTriggerCallback;
			yourself).
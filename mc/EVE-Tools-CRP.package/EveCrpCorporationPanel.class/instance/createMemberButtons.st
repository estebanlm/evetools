factory
createMemberButtons
	^ REPanel new 
		class: 'buttons';
		add: (REButton new
			class: 'btn';
			label: 'Refresh';
			callback: [ self refresh ] asReefTriggerCallback;
			yourself).
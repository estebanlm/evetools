factory
createMembers
	^ String streamContents: [ :stream | 
		(EveCorporation unops characters 
			sorted: [ :a :b | a name < b name ])
			do: [ :each | stream nextPutAll: each name ]
			separatedBy: [ stream crlf ] ]
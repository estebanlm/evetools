factory
createTaxRate
	^ REForm new 
		add: (REGrid new 
			columns: 2;
			add: 'Tax rate: ';
			add: (RETextField new 
				value: self taxRate;
				callback: [ :v | self taxRate: v asNumber ]);
			yourself);
		add: self createTaxRateButtons;
		yourself
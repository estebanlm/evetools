factory
createTaxRateButtons
	^ REPanel new 
		class: 'buttons';
		add: (REButton new
			class: 'btn btn-danger';
			label: 'Change!';
			callback: [] asReefTriggerCallback;
			yourself).
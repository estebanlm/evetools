initialization
initializeContents 
	self class: 'row'.
	self class: 'corporation'.

	self add: self createTaxRate.
	
	self add: (REForm new 
		add: ((REHeading with: 'Pilots') level: 4);
		add: (RETextArea new on: #membersText of: self);
		add: self createMemberButtons;
		yourself).

	self add: (REForm new 
		add: ((REHeading with: 'Alts') level: 4);
		add: (REGrid new 
			columns: 3;
			addAll: (self members 
				collect: [ :each | self createAltBox: each ]));
		add: self createAltButtons;
		yourself)
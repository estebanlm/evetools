factory
createCharacterPanel
	| character |
	
	character := EveConnect character.
	^ REPanel new 
		class: 'pull-right';
		class: 'character';
		add: character name;
		add: (REImage url: character urlPortrait64);
		yourself
		
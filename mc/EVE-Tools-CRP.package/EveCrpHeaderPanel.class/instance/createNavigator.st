factory
createNavigator
	^ RESimpleNavigationBar new
		addBefore: (REAnchor new 
			class: 'brand';
			label: EveCrpApplication applicationTitle;
			yourself);
		add: (REAnchor new 
			label: 'New Bill';
			url: (self basePathFor: EveCrpNewBillPanel);
			yourself);
		add: (REAnchor new 
			label: 'Week';
			url: (self basePathFor: EveCrpWeekPanel);
			yourself);
		add: (RESimpleDropdown new 
			title: 'POS';
			add: (REAnchor new 
				label: 'Shopping list';
				url: (self basePathFor: EveCrpShoppingListPanel);
				yourself);
			"add: (REAnchor new 
				label: 'Production orders';
				url: (self basePathFor: EveUNOPSProductionOrderPanel);
				yourself);"
			yourself);
		add: [ 
			REAnchor new 
				label: 'Corporation';
				url: (self basePathFor: EveCrpCorporationPanel);
				yourself ] 
			if: EveConnect character isSuperuser;
		addAfter: self createCharacterPanel;
		yourself
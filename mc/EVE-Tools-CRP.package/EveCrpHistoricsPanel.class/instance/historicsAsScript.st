accessing
historicsAsScript
	^ String streamContents: [ :stream |
			stream << '['.
			stream << '[ "Week", "ISK (M)" ],'.
			self historics 
				do: [ :each |
					stream << '[ "' << each first asString << '",' << each second asISKMillions asString << ']' ]
				separatedBy: [  
					stream << ',' ].
			stream << ']'. ]
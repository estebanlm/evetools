style
styleLess
^ '
@linkColor: rgb(51, 181, 229);
@buttonActiveBackgroundColor: #2e2e2e; //#4d4d4d;
@buttonActiveColor: #000000;
 
body { 
	font-size: 0.8em;
}

a {
	cursor: pointer;
}

div[class*="span"] {
	margin-left: 10px;
	margin-right: 10px;
}

.reefErrors {
	ul {
		list-style: none;
		margin-left: 0px;
		margin-bottom: 0px;
	}	
}

.container {
	table { 
		th {  
			padding: 3px; 
		}

		td {  
			padding: 3px; 
		}
	}
	
	.alert { 
		padding-top: 3px;
		padding-bottom: 3px;
	} 
	
	&.general-error { 
		margin-top: 100px;

		.alert { 
			padding: 20px;
		} 
	}
}

.btn.active {
	color: @buttonActiveColor;
	background-color: @buttonActiveBackgroundColor;
}

.button { 
	margin-right: 5px;
}

.header { 
	.character {
		span {
			margin-right: 10px;
			font-size: 1.2em;
		}
		
		img { 
	 		max-width: 48px;	
		}
	}
}

.bill {
	.note { 
		input { 
			width: 60%;
		}
	}
	
	.fleet {
		.controls {
			div {
				margin-bottom: 3px;
			
				input { 
					margin-bottom: 6px;
				}
				
				label {
					display: inline;
					margin-left: 5px;
				}
			}
		}
	}
}

.week { 
	table { 
		tr {
			td {
				&.c1 { 
					width: 200px;
				}
				
				&.c2 { 
					color: #ffffff;
					fond-weight: bold;
				}
			}
		}
	}
}

.corporation { 
	textarea {
		width: 100%;
		height:350px;
	}
	
	table { 
		width: 100%;

		label {
			display: inline;
			margin-left: 5px;
		}
	}
}
'
factory
createBillForm
	^ self newBill asReef
		addButtons: { #save->'Accept' };
		onAnswer: [ :v | v ifNotNil: [ self storeBill: v] ];
		yourself
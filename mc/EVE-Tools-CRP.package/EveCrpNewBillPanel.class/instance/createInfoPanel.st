initialization
createInfoPanel
	^ REPanel new 
		class: 'alert alert-info';
		add: ('Taxes are currently at {1}%.' format: { EveCrpBill taxRate });
		yourself
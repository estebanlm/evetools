initialization
initializeContents 
	self class: 'row'.
	self class: 'bill'.
	
	self add:('Add new bill' asReefHeading level: 3).
	self add: self createInfoPanel.
	self add: ((billPanel := RESimpleSpan span: 12)
		add: self createBillForm;
		yourself)
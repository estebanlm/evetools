actions
storeBill: aBill
	[ 
	self currentWeek 
		addBill: aBill;
		save.
		self 
			inform: 'You bill has been stored, wait for the weekly payment :)' 
			onAccept: [ self show: EveCrpNewBillPanel new ] ]
	on: Error, AssertionFailure do: [ :e |
		self inform: e messageText ]
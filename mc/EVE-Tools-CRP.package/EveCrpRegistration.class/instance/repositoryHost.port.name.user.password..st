accessing
repositoryHost: nameString port: portNumber name: databaseString user: userString password: passwordString
	repository := EveModel 
		host: nameString 
		port: (portNumber ifNil: [ EveModel defaultPort ])
		database: databaseString
		username: userString
		password: passwordString
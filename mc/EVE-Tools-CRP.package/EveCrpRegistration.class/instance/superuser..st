accessing
superuser: aString
	| character | 
	self validateRepository.
	character := (self repository 
		selectOne: EveCharacter 
		where: { #name -> aString } asDictionary)
		ifNil: [ 
			(EveCharacter name: aString)
				updateFromEve;
				save ].

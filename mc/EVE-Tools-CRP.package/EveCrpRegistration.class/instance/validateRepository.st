private
validateRepository
	self 
		assert: self repository notNil
		description: 'No repository defined. Declare #repositoryHost:name: before!'
paths
corporation
	<get>
	<path: 'corporation'>

	self validateSuperuser
	 ifFalse: [ ^self notFound ].

	self continueWithRoot: [ :root | 
		root startOn: EveCrpCorporationPanel new ].

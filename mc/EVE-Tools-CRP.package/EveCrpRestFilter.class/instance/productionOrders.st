paths
productionOrders
	<get>
	<path: 'production-orders'>

	self continueWithRoot: [ :root | 
		root startOn: EveCrpProductionOrderPanel new ].

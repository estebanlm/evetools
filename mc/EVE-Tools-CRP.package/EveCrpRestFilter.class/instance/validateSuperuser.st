private
validateSuperuser
	^ EveConnect isIGB 
		and: [ EveConnect character isSuperuser ]
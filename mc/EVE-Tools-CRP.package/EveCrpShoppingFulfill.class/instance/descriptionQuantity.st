description
descriptionQuantity
	<magritteDescription>
	
	^ MANumberDescription new 
		priority: 10;
		label: 'Price';
		accessor: #price;
		beRequired;
		yourself 
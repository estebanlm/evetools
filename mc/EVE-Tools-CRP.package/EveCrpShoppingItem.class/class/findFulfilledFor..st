accessing
findFulfilledFor: aCharacter
	^ self 
		selectMany: { 
			'fulfilled' -> true.
			'character.__id' -> aCharacter voyageId.
			'fulfillDate' -> { '$gte' -> (TimeStamp today - self fulfilledDaysToShow days) } asDictionary } asDictionary 
		sortBy: { #date -> VOOrder descending }  asDictionary
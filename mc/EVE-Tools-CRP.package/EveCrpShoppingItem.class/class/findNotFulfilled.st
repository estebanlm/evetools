accessing
findNotFulfilled
	^ self 
		selectMany: { #fulfilled -> false } 
		sortBy: { #date -> VOOrder descending }
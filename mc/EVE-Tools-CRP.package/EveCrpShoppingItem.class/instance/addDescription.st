description
addDescription 
	^ self descriptionContainer 
		cssClassForForm: '';
		add: self descriptionItemName;
		add: self descriptionQuantity;
		yourself
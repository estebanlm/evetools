description
descriptionItemName
	<magritteDescription>
	
	^ MAStringDescription new 
		priority: 10;
		label: 'Item';
		accessor: #itemName;
		beRequired;
		yourself 
description
descriptionQuantity
	<magritteDescription>
	
	^ MANumberDescription new 
		priority: 20;
		label: 'Quantity';
		accessor: #quantity;
		beRequired;
		yourself 
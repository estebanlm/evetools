actions
addItem 
	| item |
	
	item := self newItem.
	RESimpleDialog 
		requestMagritte: (item addDescription asReefOn: item)
		title: 'Demand item' 
		onAccept: [ :v | self storeItem: v ].
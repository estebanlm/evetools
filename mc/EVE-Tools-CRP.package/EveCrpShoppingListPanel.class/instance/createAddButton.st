factory
createAddButton 
	^ REAnchor new 
		class: 'btn btn-primary';
		label: 'Demand item';
		callback: [ self addItem ];
		yourself
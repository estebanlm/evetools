factory
createAddItemPanel
	| item |
	
	item := self newItem.
	^ (item addDescription asReefOn: item)
		addButtons: { #save->'Add to list' };
		onAnswer: [ :v | v ifNotNil: [ self storeItem: v] ];
		yourself
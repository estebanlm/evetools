factory
createFulfilledListGrid
	| grid |
	
	grid := RESimpleGrid new 
		columns: 5;
		yourself.
	
	(self fulfilledList sorted: [ :a :b | a fulfillDate > b fulfillDate ])
		do: [ :each |
			grid 
				add: each date;
				add: each itemName;
				add: each quantity;
				add: each fulfillDate;
				add: each fulfillCharacter name ].
		
	^ grid
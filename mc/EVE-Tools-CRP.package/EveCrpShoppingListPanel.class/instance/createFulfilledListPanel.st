factory
createFulfilledListPanel
	^ self hasFulfilledItems 
		ifTrue: [ self createFulfilledListGrid ]
		ifFalse: [ 'No items fulfilled at the moment' ]
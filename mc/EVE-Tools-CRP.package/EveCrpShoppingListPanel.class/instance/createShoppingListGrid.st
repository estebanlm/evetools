factory
createShoppingListGrid
	| grid |
	
	grid := RESimpleGrid new 
		columns: 5;
		yourself.
	
	self shoppingList do: [ :each |
		grid 
			add: each date;
			add: each character name;
			add: each itemName;
			add: each quantity;
			add: (REAnchor new 
				class: 'btn';
				label: 'Fulfill';
				callback: [ self fulfillItem: each ];
				yourself) ].
		
	^ grid
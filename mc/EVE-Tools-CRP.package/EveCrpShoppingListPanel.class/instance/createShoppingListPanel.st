factory
createShoppingListPanel
	^ self hasItems 
		ifTrue: [ self createShoppingListGrid ]
		ifFalse: [ 'No items demanded at the moment' ]
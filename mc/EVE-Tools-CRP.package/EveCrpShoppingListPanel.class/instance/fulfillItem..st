actions
fulfillItem: anItem

	RESimpleDialog 
		requestMagritte: EveCrpShoppingFulfill new asReef 
		title: ('Fulfill: {2} {1}' format: { anItem itemName. anItem quantity }) 
		onAccept: [ :v | 
			anItem 
				fulfillAtPrice: v price;
				save. 
			self refreshPanels ].
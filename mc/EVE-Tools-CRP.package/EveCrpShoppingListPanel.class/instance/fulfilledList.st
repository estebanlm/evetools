accessing
fulfilledList 
	^ fulfilledList ifNil: [ fulfilledList := EveCrpShoppingItem findFulfilledForCurrentCharacter ].
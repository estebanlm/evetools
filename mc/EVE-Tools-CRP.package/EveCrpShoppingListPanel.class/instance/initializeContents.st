initialization
initializeContents 
	self class: 'row'.
	self class: 'shopping-list'.

	self add: ('Items requested by me and fulfilled' asReefHeading level: 4).
	self add: ('Showing last {1} days' 
		format: { EveCrpShoppingItem fulfilledDaysToShow }) 
		asReefEmphasized.
	self add: (fulfilledListPanel := REPanel with: self createFulfilledListPanel).
	self add: self createAddButton.
	self add: ('Items demanded' asReefHeading level: 4).
	self add: (shoppingListPanel := REPanel with: self createShoppingListPanel).
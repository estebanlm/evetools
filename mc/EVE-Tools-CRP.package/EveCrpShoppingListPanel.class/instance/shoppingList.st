accessing
shoppingList 
	^ shoppingList ifNil: [ shoppingList := EveCrpShoppingItem findNotFulfilled ].
accessing
findByName: aString ifAbsent: absentBlock 
	^self all 
		detect: [ :each | each name = aString ]
		ifNone: absentBlock

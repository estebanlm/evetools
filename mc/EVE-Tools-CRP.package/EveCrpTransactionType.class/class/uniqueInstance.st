instance creation
uniqueInstance
	self = EveCrpTransactionType 
		ifTrue: [  self error: 'I''m an abstract type, please use one of my children' ].
		
	^ uniqueInstance ifNil: [ uniqueInstance := self basicNew initialize ]
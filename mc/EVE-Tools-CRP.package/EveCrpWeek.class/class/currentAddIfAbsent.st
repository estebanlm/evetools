accessing
currentAddIfAbsent
	^ self 
		findByWeek: Week current 
		ifAbsent: [ (EveCrpWeek week: Week current) save ]
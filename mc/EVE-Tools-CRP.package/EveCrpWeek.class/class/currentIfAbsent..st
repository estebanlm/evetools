accessing
currentIfAbsent: absentBlock
	^ self 
		findByWeek: Week current 
		ifAbsent: absentBlock
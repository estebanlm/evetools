accessing
findByWeek: aWeek ifAbsent: absentBlock
	| dateBegin dateEnd |
	
	dateBegin := aWeek asDate.
	dateEnd := dateBegin + 7 days.
	^ (self selectOne: [ :each | (each week >= dateBegin) & (each week < dateEnd) ])
		ifNil: absentBlock
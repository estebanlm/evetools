accessing
findOpen
	^ self selectMany: [ :each | each closed = false ]
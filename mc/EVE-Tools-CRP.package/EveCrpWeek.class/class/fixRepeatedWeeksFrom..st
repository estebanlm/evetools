tools
fixRepeatedWeeksFrom: week 
	| weeks dateBegin dateEnd |

	dateBegin := week asDate.
	dateEnd := dateBegin + 7 days.
	weeks := self selectMany: [ :each | (each week >= dateBegin) & (each week < dateEnd) ].
	weeks size > 1 ifTrue: [ 
		| keep |
		keep := weeks first.
		weeks allButFirst do: [ :each | 
			keep bills addAll: each bills.
			each remove ]. 
		keep save ].




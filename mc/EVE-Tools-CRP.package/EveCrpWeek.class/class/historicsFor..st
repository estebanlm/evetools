accessing
historicsFor: aCharacter
	| lastThreeMonths |
	
	lastThreeMonths := Month current previous previous  previous asDate.
	^ ((self selectMany: [ :each | each week >= lastThreeMonths ]) 
		sorted: [ :a :b | a week < b week ])
		collect: [ :eachWeek | 
			Array 
				with: eachWeek
		  		with: ((eachWeek bills
					collect: [ :eachBill | eachBill shareFor: EveConnect character ])
						ifNotEmpty: [ :bills | bills sum ] 
						ifEmpty: [ 0 ]) ].

persistence
mongoWeek 
	<mongoDescription>
	
	^ VOMongoToOneDescription new 
		attributeName: 'week';
		accessor: (MAPluggableAccessor 
			read: [ :week | week week asDate ] 
			write: [ :week :value | week week: value asDate asWeek ]);
		yourself
accessing
addBill: aBill
	self 
		assert: self isOpen 
		description: 'You cannot add bills to a closed week!'.
	^ self bills add: aBill
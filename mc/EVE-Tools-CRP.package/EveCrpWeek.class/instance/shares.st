accessing
shares
	| shares |
	
	shares := Dictionary new.
	self bills do: [ :eachBill |
		eachBill fleet do: [ :eachCharacter |
			shares at: eachCharacter ifAbsentPut: [ 0 ].
			shares at: eachCharacter put: (shares at: eachCharacter) + (eachBill shareFor: eachCharacter) ] ].
	
	^ shares
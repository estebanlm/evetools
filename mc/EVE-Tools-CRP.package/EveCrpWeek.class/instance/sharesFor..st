accessing
sharesFor: aCharacter
	^ self bills select: [ :each | each includesCharacter: aCharacter ]
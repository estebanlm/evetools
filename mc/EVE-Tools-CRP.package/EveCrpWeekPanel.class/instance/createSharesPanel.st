factory
createSharesPanel
	| grid total |
	
	grid := RESimpleGrid new 
		columns: 4;
		yourself.
		
	total := 0.
	self shares do: [ :each |
		| share |
		share := each shareFor: EveConnect character.
		total := total + share.
		grid 
			add: each date;
			add: share asISKMillionsString;
			add: each conceptName;
			add: each fleetAsCommaString.
		each comment isEmptyOrNil 
			ifFalse: [ 
				grid addAll: { ''. ''. ''. each comment } ] ].
		
	^ REContainer new 
		add: grid;
		add: (REHeading new 
			level: 4;
			text: (REContainer new 
				add: 'Total: ' asReefStrong;
				add: total asISKMillionsString;
				yourself));
		yourself
	
	
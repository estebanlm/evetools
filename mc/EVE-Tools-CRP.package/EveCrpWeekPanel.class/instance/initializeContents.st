initialization
initializeContents 
	self class: 'row'.
	self class: 'week'.
	
 	self add: (self currentWeek asString asReefHeading level: 3).
	self add: (self shares 
		ifNotEmpty: [ self createSharesPanel ]
		ifEmpty: [ 'No reports of you this week' ]).
	self add: self createHistoricsPanel.

	self add: [ self createTotalsPanel ] if: self isSuperuser.
		
		
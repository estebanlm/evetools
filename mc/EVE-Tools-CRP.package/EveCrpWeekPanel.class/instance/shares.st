accessing
shares
	^ shares ifNil: [ 
		shares := self currentWeek sharesFor: EveConnect character ]
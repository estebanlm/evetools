actions
closeWeek: aWeek
	self 
		confirm: 'Are you sure?'
		onAccept: [
			aWeek 
				close; 
				save.
			self refreshTotalsPanel ]
factory
createCorporationPanelFor: week
	| grid total |
	
	grid := RESimpleGrid new 
		columns: 4;
		yourself.
		
	total := 0.
	week bills do: [ :each |
		| share |
		share := each corporationShare.
		total := total + share.
		grid 
			add: each date;
			add: share asISKMillionsString;
			add: each conceptName;
			add: each fleetAsCommaString ].
		
	^ REContainer new 
		add: grid;
		add: (REPanel new 
			add: 'Total: ' asReefStrong;
			add: total asISKMillionsString;
			yourself);
		yourself
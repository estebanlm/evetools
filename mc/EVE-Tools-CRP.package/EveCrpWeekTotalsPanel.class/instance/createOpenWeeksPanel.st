factory
createOpenWeeksPanel
	| panel |
	
	panel := RESimpleTabs new. 
	
	self openWeeks do: [ :eachWeek |
		panel 
			at: eachWeek asString 
			put: (
				REPanel new
					add: ('Corporation' asReefHeading level: 4); 
					add: (self createCorporationPanelFor: eachWeek);
					add: ('Shares' asReefHeading level: 4); 
					add: (self createSharesPanelFor: eachWeek);
					add: (REAnchor new 
						class: 'btn btn-primary';
						label: 'Close this week';
						callback: [ self closeWeek: eachWeek ];
						yourself);
					yourself) ].

	^ panel
factory
createSharesPanelFor: week
	| grid total |
	
	grid := RESimpleGrid new 
		columns: 2;
		yourself.

	total := 0.		
	(week shares associations 
		sorted: [ :a :b | a key plainName < b key plainName ]) 
		do: [ :each |
			| shares |
			shares := each value.
			total := total + shares.
			grid 
				add: each key name;
				add: shares asISKMillionsString ].

	^ REContainer new 
		add: grid;
		add: (REPanel new 
			add: 'Total: ' asReefStrong;
			add: total asISKMillionsString;
			yourself);
		yourself
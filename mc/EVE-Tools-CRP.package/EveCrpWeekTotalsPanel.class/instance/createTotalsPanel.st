factory
createTotalsPanel
	^ self openWeeks 
		ifNotEmpty: [ self createOpenWeeksPanel ]
		ifEmpty: [ 'No open weeks' ]
initialization
initializeContents 
	self class: 'totals'.
	
	self add: ('Totals' asReefHeading level: 3).
	self add: ((totalPanel := REPanel new) 
		add: self createTotalsPanel;
		yourself)
		
*EVE-Tools-Core
asHumanReadableString
	| years months days result |

	days := self days.
	years := (days // 30) // 12.
	months := (days - (years * 365)) // 30.
	
	result := OrderedCollection new.
	years > 0 ifTrue: [ 
		result add: (years asString, (years > 1 ifTrue: [ ' years' ] ifFalse: [ ' year' ])) ].
	months > 0 ifTrue: [ 
		result add: (months asString, (months > 1 ifTrue: [ ' months' ] ifFalse: [ ' month' ])) ].
	
	^ result 
		ifNotEmpty: [ result asCommaString ]
		ifEmpty: [ 'less than one month' ]
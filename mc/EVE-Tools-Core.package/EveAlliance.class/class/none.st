accessing
none
	^self voyageRepository
		secundaryCacheAt: #AllianceNone
		ifAbsentPut: [ 
			(self selectOne: [ :each | each name = 'None' ] asMongoQuery)
				ifNil: [ (self name: 'None') save ] ]
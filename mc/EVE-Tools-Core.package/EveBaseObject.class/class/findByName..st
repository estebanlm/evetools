accessing
findByName: aString
	^self 
		findByName: aString 
		ifAbsent: [ NotFound signal: aString, ' not found.' ] 
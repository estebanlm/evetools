accessing
findByName: aString ifAbsent: absentBlock 
	| lower |
	lower := aString asLowercase.
	^ (self selectOne: { 
		#name -> { '$regex' -> lower. '$options' -> 'i' } asDictionary. 
		} asDictionary)
		ifNil: absentBlock

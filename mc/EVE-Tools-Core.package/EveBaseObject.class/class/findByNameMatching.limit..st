accessing
findByNameMatching: aString limit: limitNumber
	| result regex |
	
	
	regex := '^', aString asLowercase, '.*'.
	^ self 
		selectMany: { #name -> { '$regex' -> regex. '$options' -> 'i' } asDictionary } asDictionary
		limit: limitNumber
accessing
nameWithLabel
	^String streamContents: [ :stream |
		stream 
			<< self name
			<< ' ('
			<< self label 
			<< ')' ]
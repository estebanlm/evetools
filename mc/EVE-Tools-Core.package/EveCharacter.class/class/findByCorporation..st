accessing
findByCorporation: aCorporation
	| id |
	
	id := self voyageRepository keyOf: aCorporation.
	^ self selectMany: [ :each | (each at: 'corporation.__id') = id ] asMongoQuery
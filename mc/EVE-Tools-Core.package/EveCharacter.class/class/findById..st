accessing
findById: id
	^ self 
		findById: id 
		ifAbsent: [ NotFound signal: 'Character ', id, ' not found.' ]
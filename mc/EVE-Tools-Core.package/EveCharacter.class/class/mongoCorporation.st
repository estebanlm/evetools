persistence
mongoCorporation
	<mongoDescription>
	
	^ VOMongoToOneDescription new 
		attributeName: 'corporation';
		kind: EveCorporation;
		yourself
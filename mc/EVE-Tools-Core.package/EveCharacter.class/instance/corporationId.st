accessing
corporationId
	^ self corporation 
		ifNotNil: [ self corporation id ]
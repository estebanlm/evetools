accessing
corporationName
	^ self corporation 
		ifNotNil: [ self corporation name ]
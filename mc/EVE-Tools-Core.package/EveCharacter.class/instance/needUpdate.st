testing
needUpdate
	^ lastUpdate isNil 
		or: [ (TimeStamp now - lastUpdate) > self class updateDuration ]
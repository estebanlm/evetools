accessing
roundedSecurityStatus
	^ ((self securityStatus * 100) asInteger / 100) asFloat 
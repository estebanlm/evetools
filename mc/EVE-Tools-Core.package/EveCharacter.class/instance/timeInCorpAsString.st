accessing
timeInCorpAsString
	^ (TimeStamp now - self corporationDate) asHumanReadableString 
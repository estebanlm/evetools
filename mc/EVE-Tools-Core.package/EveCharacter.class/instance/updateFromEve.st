remote update
updateFromEve
	| connect xml result corpId allianceId employmentHistory lastEmployment |
	
	connect := EveConnect new.
	(id isNil or: [ id = 0 ]) ifTrue: [ 
		xml := connect characterIdsFor: { self name }.
		id := ((xml findXPath: '/eveapi/result/rowset/row[1]') attributeAt: 'characterID') asNumber.
		id = 0 ifTrue: [ EveError signal: ('Character {1} does not exits.' format: { self name  }) ] ].
	
	xml := connect characterInfoFor: id.
	result := xml findXPath: '/eveapi/result'.
	
	securityStatus := (result findXPath: 'securityStatus') contentText asNumber.
	employmentHistory := result findXPath: 'rowset[@name="employmentHistory"]/row'.
	lastEmployment := employmentHistory isCollection 
		ifTrue: [ employmentHistory last ]
		ifFalse: [ employmentHistory ].
	startDate := TimeStamp fromString: (lastEmployment attributeAt: 'startDate').
	
	corpId := (result findXPath: 'corporationID') contentText asNumber.
	corporation := EveCorporation 
		findById: corpId 
		ifAbsent: [ 
			EveCorporation 
				id: corpId
				name: ((result findXPath: 'corporation') contentText) ].
	corporationDate := TimeStamp fromString: (result findXPath: 'corporationDate') contentText.
	
	(result findXPath: 'allianceID') 
		ifNotNil: [ :allianceElement |
			allianceId :=  allianceElement contentText asNumber.
			alliance := EveAlliance
				findById: allianceId 
				ifAbsent: [ 
					EveAlliance
						id: allianceId
						name: ((result findXPath: 'alliance') contentText) ] ] 
		ifNil: [
			alliance := EveAlliance none ]. 
		
	lastUpdate := TimeStamp now.
private
urlPortrait: size 
	^ String streamContents: [ :stream |
		stream 
			nextPutAll: 'http://image.eveonline.com/Character/';
			nextPutAll: self id asString;
			nextPut: $_;
			nextPutAll: size asString;
			nextPutAll: '.jpg' ].
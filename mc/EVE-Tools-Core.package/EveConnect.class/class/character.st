accessing header
character
	| characterId character needUpdate |
	
	self isDevelopment ifTrue: [ ^ EveCharacter findByName: 'Steve Renalard' ].

	characterId := self requestHeaderAt: 'eve_charid' ifAbsent: [ ^ nil ].	
	character := (EveCharacter findById: characterId asNumber ifAbsent: [ self createCharacterFromName ]).
	needUpdate := character needUpdate 
		or: [ 
			| corporationId |
			corporationId := (self requestHeaderAt: 'eve_corpid' ifAbsent: [ 0 ]) asNumber.
			character corporationId = corporationId ].

	needUpdate ifTrue: [ character updateFromEve; save ].
private
createCharacterFromName
	| charName |
	
	charName := self requestHeaderAt: 'eve_charname' ifAbsent: [ ^nil ]. 
	^ EveCharacter name: charName
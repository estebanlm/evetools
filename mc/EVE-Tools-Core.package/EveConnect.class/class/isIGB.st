accessing header
isIGB
	self isDevelopment ifTrue: [ ^ true ].
	
	^ (self 
		requestHeaderAt: 'user-agent' 
		ifAbsent: [ '' ]) 
		endsWith: 'EVE-IGB'
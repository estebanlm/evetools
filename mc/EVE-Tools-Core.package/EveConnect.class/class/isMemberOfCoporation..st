accessing header
isMemberOfCoporation: corporationId
	self isDevelopment ifTrue: [ ^ true ].

	^	(self requestHeaderAt: 'eve_corpid' ifAbsent: [ 0 ]) asNumber = corporationId
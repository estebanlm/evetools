accessing header
isTrusted
	self isDevelopment ifTrue: [ ^ true ].

	^ (self requestHeaderAt: 'eve_trusted') = 'Yes'
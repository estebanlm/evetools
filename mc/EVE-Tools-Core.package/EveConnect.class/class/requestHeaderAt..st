private
requestHeaderAt: aString
	^self requestHeaderAt: aString ifAbsent: [ nil ]
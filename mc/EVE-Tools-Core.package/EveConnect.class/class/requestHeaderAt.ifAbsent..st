private
requestHeaderAt: aString ifAbsent: absentBlock
	^self requestContext request 
		headerAt: aString 
		ifAbsent: absentBlock
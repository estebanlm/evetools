eve api
characterIdsFor: aCollection 
	"http://wiki.eve-id.net/APIv2_Eve_CharacterID_XML"
	| names |
	
	names := String streamContents: [ :stream |
		aCollection asStringOn: stream  delimiter: ','].
	
	^self 
		parseRequest: 'CharacterID.xml.aspx'
		query: { #names->names } asDictionary
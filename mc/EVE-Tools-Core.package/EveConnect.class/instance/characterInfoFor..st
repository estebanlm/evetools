eve api
characterInfoFor: idNumber 
	"http://wiki.eve-id.net/APIv2_Eve_CharacterInfo_XML"
	
	^ self 
		parseRequest: 'CharacterInfo.xml.aspx'
		query: { 'characterID' -> idNumber asString } asDictionary
		
private
parseRequest: aString query: aDictionary
	^ self parse: (ZnClient new 
		setAcceptEncodingGzip;
		url: (self urlFor: aString);
		queryAddAll: aDictionary;
		get)
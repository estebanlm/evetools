private
urlFor: aString
	"Answer a general url for eve api (a general url is started with /eve/)"
	^(ZnUrl fromString: 'https://api.eveonline.com')
		addPathSegment: 'eve';
		addPathSegment: aString;
		yourself
accessing
nextTrackId
	| id |

	[ id := EveUniqueIdGenerator uniqueInstance nextId.
	  self trackExists: id ]
	whileTrue.
	
	^ id	
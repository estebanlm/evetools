accessing
oldNextTrackId
	currentTrackId ifNil: [ currentTrackId := 0 ].
	currentTrackId := currentTrackId + 1.
	
	^ self toString36: currentTrackId
	
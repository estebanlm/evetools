private
toString36: aNumber
	| bytes number |
	bytes := (aNumber asString padLeftTo: 8 with: $0) asByteArray. 
	number := 0.
	1 to: bytes size do: [:i | 
		number := number + ((256 raisedTo: i - 1) * (bytes at: i)) ].
	
	^(number printStringBase: 36) asLowercase
	
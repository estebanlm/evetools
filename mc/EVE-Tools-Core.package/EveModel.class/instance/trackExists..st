accessing
trackExists: id
	^ (self selectOne: EveTrack where: { #id -> id } asDictionary) notNil.
accessing
findById: id
	^ self 
		findById: id 
		ifAbsent: [ NotFound signal: (self name asString allButFirst: 3), ' ', id, ' not found.' ]
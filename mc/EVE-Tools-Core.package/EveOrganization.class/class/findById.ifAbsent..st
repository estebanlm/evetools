accessing
findById: id ifAbsent: absentBlock
	^ (self selectOne: [ :each | each id = id ] asMongoQuery)
		ifNil: absentBlock 
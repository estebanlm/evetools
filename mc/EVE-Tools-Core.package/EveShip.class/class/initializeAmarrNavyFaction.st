as yet unclassified
initializeAmarrNavyFaction
	
	"Frigates"	
	(self name: 'Amarr navy slicer') 
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel faction;
		race: EveRace amarr;
		save. 
	(self name: 'Gold magnate')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel faction;
		race: EveRace amarr;
		save. 
	(self name: 'Imperial navy slicer') 
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel faction;
		race: EveRace amarr;
		save. 
	(self name: 'Silver magnate') 
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel faction;
		race: EveRace amarr;
		save.
	"Cruisers"
	(self name: 'Augoror navy issue')  
		shipClass: (EveShipClass findByName: 'Cruiser');
		techLevel: EveTechLevel faction;
		race: EveRace amarr;
		save. 
	(self name: 'Omen navy issue')  
		shipClass: (EveShipClass findByName: 'Cruiser');
		techLevel: EveTechLevel faction;
		race: EveRace amarr;
		save. 
	"Battleships"
	(self name: 'Apocalypse imperial issue')  
		shipClass: (EveShipClass findByName: 'Battleship');
		techLevel: EveTechLevel faction;
		race: EveRace amarr;
		save. 
	(self name: 'Apocalypse navy issue')  
		shipClass: (EveShipClass findByName: 'Battleship');
		techLevel: EveTechLevel faction;
		race: EveRace amarr;
		save. 
	(self name: 'Armageddon imperial issue')  
		shipClass: (EveShipClass findByName: 'Battleship');
		techLevel: EveTechLevel faction;
		race: EveRace amarr;
		save. 
	(self name: 'Armageddon navy issue')  
		shipClass: (EveShipClass findByName: 'Battleship');
		techLevel: EveTechLevel faction;
		race: EveRace amarr;
		save.
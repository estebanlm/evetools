as yet unclassified
initializeCaldariNavyFaction
	
	"Frigates"	
	(self name: 'Caldari Navy Hookbill')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel faction;
		race: EveRace caldari;
		save. 
	"Cruisers"
	(self name: 'Caracal navy issue')  
		shipClass: (EveShipClass findByName: 'Cruiser');
		techLevel: EveTechLevel faction;
		race: EveRace caldari;
		save. 
	(self name: 'Osprey navy issue')  
		techLevel: EveTechLevel faction;
		race: EveRace caldari;
		save. 
	"Battleships"
	(self name: 'Raven navy issue')  
		shipClass: (EveShipClass findByName: 'Battleship');
		techLevel: EveTechLevel faction;
		race: EveRace caldari;
		save. 
	(self name: 'Scorpion navy issue')  
		shipClass: (EveShipClass findByName: 'Battleship');
		techLevel: EveTechLevel faction;
		race: EveRace caldari;
		save. 
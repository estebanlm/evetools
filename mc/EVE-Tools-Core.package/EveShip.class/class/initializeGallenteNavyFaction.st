as yet unclassified
initializeGallenteNavyFaction
	
	"Frigates"	
	(self name: 'Federation navy comet')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel faction;
		race: EveRace gallente;		
		save. 
	(self name: 'Inner Zone Shipping Imicus')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel faction;
		race: EveRace gallente;		
		save. 
	(self name: 'Utu')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel faction;
		race: EveRace gallente;		
		save. 
	"Cruisers"
	(self name: 'Adrestia')  
		shipClass: (EveShipClass findByName: 'Cruiser');
		techLevel: EveTechLevel faction;
		race: EveRace gallente;		
		save. 
	(self name: 'Exequror navy issue')  
		shipClass: (EveShipClass findByName: 'Cruiser');
		techLevel: EveTechLevel faction;
		race: EveRace gallente;		
		save. 
	(self name: 'Guardian-vexor')  
		shipClass: (EveShipClass findByName: 'Cruiser');
		techLevel: EveTechLevel faction;
		race: EveRace gallente;		
		save. 
	(self name: 'Vexor navy issue')  
		shipClass: (EveShipClass findByName: 'Cruiser');
		techLevel: EveTechLevel faction;
		race: EveRace gallente;		
		save. 
	"Battleships"
	(self name: 'Dominix navy issue')  
		shipClass: (EveShipClass findByName: 'Battleship');
		techLevel: EveTechLevel faction;
		race: EveRace gallente;		
		save. 
	(self name: 'Megathron navy issue')  
		shipClass: (EveShipClass findByName: 'Battleship');
		techLevel: EveTechLevel faction;
		race: EveRace gallente;		
		save. 
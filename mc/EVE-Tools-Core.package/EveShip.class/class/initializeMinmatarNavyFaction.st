as yet unclassified
initializeMinmatarNavyFaction
	
	"Frigates"	
	(self name: 'Freki')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel faction;
		race: EveRace minmatar;		
		save. 
	(self name: 'Republic fleet firetail')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel faction;
		race: EveRace minmatar;		
		save. 
	"Cruisers"
	(self name: 'Mimir')  
		shipClass: (EveShipClass findByName: 'Cruiser');
		techLevel: EveTechLevel faction;
		race: EveRace minmatar;		
		save. 
	(self name: 'Scythe fleet issue')  
		shipClass: (EveShipClass findByName: 'Cruiser');
		techLevel: EveTechLevel faction;
		race: EveRace minmatar;		
		save. 
	(self name: 'Stabber fleet issue')  
		shipClass: (EveShipClass findByName: 'Cruiser');
		techLevel: EveTechLevel faction;
		race: EveRace minmatar;		
		save. 
	"Battleships"
	(self name: 'Tempest fleet issue')  
		shipClass: (EveShipClass findByName: 'Battleship');
		techLevel: EveTechLevel faction;
		race: EveRace minmatar;		
		save. 
	(self name: 'Typhoon fleet issue')  
		shipClass: (EveShipClass findByName: 'Battleship');
		techLevel: EveTechLevel faction;
		race: EveRace minmatar;		
		save. 
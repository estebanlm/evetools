as yet unclassified
initializeOREShips

	"Frigate"
	(self name: 'Venture')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel t1;
		race: EveRace any;						
		"addRole: (EveShipRole name: 'Miner');"
		save. 	
	"Mining Barge"
	(self name: 'Covetor')  
		shipClass: (EveShipClass findByName: 'Mining Barge');
		techLevel: EveTechLevel t1;
		race: EveRace any;						
		save. 
	(self name: 'Procurer')  
		shipClass: (EveShipClass findByName: 'Mining Barge');
		techLevel: EveTechLevel t1;
		race: EveRace any;						
		save. 
	(self name: 'Retriever')  
		shipClass: (EveShipClass findByName: 'Mining Barge');
		techLevel: EveTechLevel t1;
		race: EveRace any;						
		save. 
	"Exhumers"
	(self name: 'Hulk')  
		shipClass: (EveShipClass findByName: 'Exhumer');
		techLevel: EveTechLevel t1;
		race: EveRace any;						
		save. 
	(self name: 'Mackinaw')  
		shipClass: (EveShipClass findByName: 'Exhumer');
		techLevel: EveTechLevel t1;
		race: EveRace any;						
		save. 
	(self name: 'Skiff')  
		shipClass: (EveShipClass findByName: 'Exhumer');
		techLevel: EveTechLevel t1;
		race: EveRace any;						
		save. 
	"Industrials"
	(self name: 'Noctis')  
		shipClass: (EveShipClass findByName: 'Industrial');
		techLevel: EveTechLevel t1;
		race: EveRace any;						
		save. 
	(self name: 'Primae')  
		shipClass: (EveShipClass findByName: 'Industrial');
		techLevel: EveTechLevel t1;
		race: EveRace any;						
		save. 
	"Capital Industrial Ships"
	(self name: 'Orca')  
		shipClass: (EveShipClass findByName: 'Capital Industrial');
		techLevel: EveTechLevel t1;
		race: EveRace any;						
		save. 
	(self name: 'Rorqual')  
		shipClass: (EveShipClass findByName: 'Capital Industrial');
		techLevel: EveTechLevel t1;
		race: EveRace any;						
		save. 
as yet unclassified
initializeOtherShips
	
	"Shuttles"
	(self name: 'Capsule')  
		shipClass: (EveShipClass findByName: 'Capsule');
		techLevel: nil;
		race: EveRace any;				
		save. 
	"Shuttles"
	(self name: 'Goru shuttle')  
		shipClass: (EveShipClass findByName: 'Shuttle');
		techLevel: EveTechLevel faction;
		race: EveRace other;				
		save. 
	(self name: 'Guristas shuttle')  
		shipClass: (EveShipClass findByName: 'Shuttle');
		techLevel: EveTechLevel faction;
		race: EveRace other;				
		save. 
	(self name: 'Interbus shuttle')  
		shipClass: (EveShipClass findByName: 'Shuttle');
		techLevel: EveTechLevel faction;
		race: EveRace other;				
		save. 
	"Frigates"
	(self name: 'Polaris Centurion')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel faction;
		race: EveRace other;				
		save. 
	(self name: 'Polaris Inspector frigate')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel faction;
		race: EveRace other;				
		save. 
	(self name: 'Polaris Legatus')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel faction;
		race: EveRace other;				
		save. 
	"Faction Frigates"
	(self name: 'Apotheosis')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel faction;
		race: EveRace other;				
		save. 
	(self name: 'Echelon')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel faction;
		race: EveRace other;				
		save. 
	(self name: 'Zephyr')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel faction;
		race: EveRace other;				
		save. 
	"Cruisers"
	(self name: 'Opux Luxury Yacht')  
		shipClass: (EveShipClass findByName: 'Cruiser');
		techLevel: EveTechLevel faction;
		race: EveRace other;				
		save. 
	"Battleships"
	(self name: 'Megathron federate issue')  
		shipClass: (EveShipClass findByName: 'Battleship');
		techLevel: EveTechLevel faction;
		race: EveRace other;				
		save. 
	(self name: 'Raven state issue')  
		shipClass: (EveShipClass findByName: 'Battleship');
		techLevel: EveTechLevel faction;
		race: EveRace other;				
		save. 
	(self name: 'Tempest tribal issue')  
		shipClass: (EveShipClass findByName: 'Battleship');
		techLevel: EveTechLevel faction;
		race: EveRace other;				
		save. 
	"Assault Ships"
	(self name: 'Cambion')  
		shipClass: (EveShipClass findByName: 'Assault Ship');
		techLevel: EveTechLevel faction;
		race: EveRace other;				
		save. 
	(self name: 'Malice')  
		shipClass: (EveShipClass findByName: 'Assault Ship');
		techLevel: EveTechLevel faction;
		race: EveRace other;				
		save. 
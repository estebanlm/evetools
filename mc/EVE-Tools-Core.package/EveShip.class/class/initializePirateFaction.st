as yet unclassified
initializePirateFaction
	
	"Frigates"	
	(self name: 'Cruor')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel faction;
		race: EveRace other;
		save. 
	(self name: 'Daredevil')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel faction;
		race: EveRace other;
		save. 
	(self name: 'Dramiel')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel faction;
		race: EveRace other;
		save. 
	(self name: 'Succubus')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel faction;
		race: EveRace other;
		save. 
	(self name: 'Worm')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel faction;
		race: EveRace other;
		save. 
	"Cruisers"
	(self name: 'Ashimmu')  
		shipClass: (EveShipClass findByName: 'Cruiser');
		techLevel: EveTechLevel faction;
		race: EveRace other;
		save. 
	(self name: 'Cynabal')  
		shipClass: (EveShipClass findByName: 'Cruiser');
		techLevel: EveTechLevel faction;
		race: EveRace other;
		save. 
	(self name: 'Gila')  
		shipClass: (EveShipClass findByName: 'Cruiser');
		techLevel: EveTechLevel faction;
		race: EveRace other;
		save. 
	(self name: 'Phantasm')  
		shipClass: (EveShipClass findByName: 'Cruiser');
		techLevel: EveTechLevel faction;
		race: EveRace other;
		save. 
	(self name: 'Vigilant')  
		shipClass: (EveShipClass findByName: 'Cruiser');
		techLevel: EveTechLevel faction;
		race: EveRace other;
		save. 
	"Battleships"
	(self name: 'Bhaalgorn')  
		shipClass: (EveShipClass findByName: 'Battleship');
		techLevel: EveTechLevel faction;
		race: EveRace other;
		save. 
	(self name: 'Machariel')  
		shipClass: (EveShipClass findByName: 'Battleship');
		techLevel: EveTechLevel faction;
		race: EveRace other;
		save. 
	(self name: 'Nightmare')  
		shipClass: (EveShipClass findByName: 'Battleship');
		techLevel: EveTechLevel faction;
		race: EveRace other;
		save. 
	(self name: 'Rattlesnake')  
		shipClass: (EveShipClass findByName: 'Battleship');
		techLevel: EveTechLevel faction;
		race: EveRace other;
		save. 
	(self name: 'Vindicator')  
		shipClass: (EveShipClass findByName: 'Battleship');
		techLevel: EveTechLevel faction;
		race: EveRace other;
		save.
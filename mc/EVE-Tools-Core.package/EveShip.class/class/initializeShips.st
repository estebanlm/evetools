as yet unclassified
initializeShips
	"T1"
	self initializeT1Shuttles.
	self initializeT1Rookie.
	self initializeT1Frigates.
	self initializeT1Destroyers.			
	self initializeT1Cruiser.
	self initializeT1Battlecruisers.
	self initializeT1Battleships.
	self initializeT1AdvancedBattlecruisers.
	self initializeT1Carrier.
	self initializeT1Dreadnoughts.
	self initializeT1Supercarriers.
	self initializeT1Titan.
	self initializeT1Industrials.
	self initializeT1Freighters.
	self initializeT1JumpFreighters.
	"T2"
	self initializeT2AssaultShips.
	self initializeT2Marauders.
	self initializeT2BlackOps.
	self initializeT2CoverOps.
	self initializeT2ElectonicAttackShips.
	self initializeT2Interceptors.
	self initializeT2Interdictors.
	self initializeT2HeavyInterdictors.
	self initializeT2HeavyAssaultShips.
	self initializeT2ReconShips.
	self initializeT2CommandShips.
	self initializeT2Transports.
	self initializeT2BlockadeRunners.
	self initializeT2Logistics.
	"T3"
	self initializeT3StrategicCruiser.
	"Faction"
	self initializeAmarrNavyFaction.
	self initializeCaldariNavyFaction.
	self initializeGallenteNavyFaction.
	self initializeMinmatarNavyFaction.
	self initializePirateFaction.
	"Others"
	self initializeOREShips.
	self initializeOtherShips.
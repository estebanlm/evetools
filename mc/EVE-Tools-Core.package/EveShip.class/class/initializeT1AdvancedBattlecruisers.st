as yet unclassified
initializeT1AdvancedBattlecruisers
	"T1 - Advanced battlecruisers"
	
	(self name: 'Oracle')  
		shipClass: (EveShipClass findByName: 'Assault Battlecruiser');
		techLevel: EveTechLevel t1;
		race: EveRace amarr;
		save. 
	(self name: 'Naga')  
		shipClass: (EveShipClass findByName: 'Assault Battlecruiser');
		techLevel: EveTechLevel t1;
		race: EveRace caldari;
		save. 
	(self name: 'Talos')  
		shipClass: (EveShipClass findByName: 'Assault Battlecruiser');
		techLevel: EveTechLevel t1;
		race: EveRace gallente;		
		save. 
	(self name: 'Tornado')  
		shipClass: (EveShipClass findByName: 'Assault Battlecruiser');
		techLevel: EveTechLevel t1;
		race: EveRace minmatar;		
		save. 
as yet unclassified
initializeT1Battlecruisers
	"T1 - Battlecruisers"
	
	(self name: 'Harbinger')  
		shipClass: (EveShipClass findByName: 'Combat Battlecruiser');
		techLevel: EveTechLevel t1;
		race: EveRace amarr;
		save. 
	(self name: 'Prophecy')  
		shipClass: (EveShipClass findByName: 'Combat Battlecruiser');
		techLevel: EveTechLevel t1;
		race: EveRace amarr;
		save. 
	(self name: 'Drake')  
		shipClass: (EveShipClass findByName: 'Combat Battlecruiser');
		techLevel: EveTechLevel t1;
		race: EveRace caldari;
		save. 
	(self name: 'Ferox')  
		shipClass: (EveShipClass findByName: 'Combat Battlecruiser');
		techLevel: EveTechLevel t1;
		race: EveRace caldari;
		save. 
	(self name: 'Brutix')  
		shipClass: (EveShipClass findByName: 'Combat Battlecruiser');
		techLevel: EveTechLevel t1;
		race: EveRace gallente;		
		save. 
	(self name: 'Myrmidon')  
		shipClass: (EveShipClass findByName: 'Combat Battlecruiser');
		techLevel: EveTechLevel t1;
		race: EveRace gallente;		
		save. 
	(self name: 'Cyclone')  
		shipClass: (EveShipClass findByName: 'Combat Battlecruiser');
		techLevel: EveTechLevel t1;
		race: EveRace minmatar;		
		save. 
	(self name: 'Hurricane')  
		shipClass: (EveShipClass findByName: 'Combat Battlecruiser');
		techLevel: EveTechLevel t1;
		race: EveRace minmatar;		
		save. 

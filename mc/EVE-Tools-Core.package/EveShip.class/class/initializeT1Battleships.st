as yet unclassified
initializeT1Battleships
	"T1 - Battleships"

	(self name: 'Abaddon')  
		shipClass: (EveShipClass findByName: 'Battleship');
		techLevel: EveTechLevel t1;
		race: EveRace amarr;
		save. 
	(self name: 'Apocalypse')  
		shipClass: (EveShipClass findByName: 'Battleship');
		techLevel: EveTechLevel t1;
		race: EveRace amarr;
		save. 
	(self name: 'Armageddon')  
		shipClass: (EveShipClass findByName: 'Battleship');
		techLevel: EveTechLevel t1;
		race: EveRace amarr;
		save. 
	(self name: 'Raven')  
		shipClass: (EveShipClass findByName: 'Battleship');
		techLevel: EveTechLevel t1;
		race: EveRace caldari;
		save. 
	(self name: 'Rokh')  
		shipClass: (EveShipClass findByName: 'Battleship');
		techLevel: EveTechLevel t1;
		race: EveRace caldari;
		save. 
	(self name: 'Scorpion')  
		shipClass: (EveShipClass findByName: 'Battleship');
		techLevel: EveTechLevel t1;
		race: EveRace caldari;
		save. 
	(self name: 'Dominix')  
		shipClass: (EveShipClass findByName: 'Battleship');
		techLevel: EveTechLevel t1;
		race: EveRace gallente;		
		save. 
	(self name: 'Hyperion')  
		shipClass: (EveShipClass findByName: 'Battleship');
		techLevel: EveTechLevel t1;
		race: EveRace gallente;		
		save. 
	(self name: 'Megathron')  
		shipClass: (EveShipClass findByName: 'Battleship');
		techLevel: EveTechLevel t1;
		race: EveRace gallente;		
		save. 
	(self name: 'Maelstrom')  
		shipClass: (EveShipClass findByName: 'Battleship');
		techLevel: EveTechLevel t1;
		race: EveRace minmatar;		
		save. 
	(self name: 'Tempest')  
		shipClass: (EveShipClass findByName: 'Battleship');
		techLevel: EveTechLevel t1;
		race: EveRace minmatar;		
		save. 
	(self name: 'Typhoon')  
		shipClass: (EveShipClass findByName: 'Battleship');
		techLevel: EveTechLevel t1;
		race: EveRace minmatar;		
		save. 
	
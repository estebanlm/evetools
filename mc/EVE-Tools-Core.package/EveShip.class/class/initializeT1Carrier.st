as yet unclassified
initializeT1Carrier
	"T1 - Carrier"

	(self name: 'Archon')  
		shipClass: (EveShipClass findByName: 'Carrier');
		techLevel: EveTechLevel t1;
		race: EveRace amarr;
		save. 
	(self name: 'Chimera')  
		shipClass: (EveShipClass findByName: 'Carrier');
		techLevel: EveTechLevel t1;
		race: EveRace caldari;
		save. 
	(self name: 'Thanatos')  
		shipClass: (EveShipClass findByName: 'Carrier');
		techLevel: EveTechLevel t1;
		race: EveRace gallente;		
		save. 
	(self name: 'Nidhoggur')  
		shipClass: (EveShipClass findByName: 'Carrier');
		techLevel: EveTechLevel t1;
		race: EveRace minmatar;		
		save. 

as yet unclassified
initializeT1Cruiser
	"T1 - Cruiser"

	(self name: 'Arbitrator')  
		shipClass: (EveShipClass findByName: 'Cruiser');
		techLevel: EveTechLevel t1;
		race: EveRace amarr;
		save. 
	(self name: 'Augoror')  
		shipClass: (EveShipClass findByName: 'Cruiser');
		techLevel: EveTechLevel t1;
		race: EveRace amarr;
		save. 
	(self name: 'Maller')  
		shipClass: (EveShipClass findByName: 'Cruiser');
		techLevel: EveTechLevel t1;
		race: EveRace amarr;
		save. 
	(self name: 'Omen')  
		shipClass: (EveShipClass findByName: 'Cruiser');
		techLevel: EveTechLevel t1;
		race: EveRace amarr;
		save.  

	(self name: 'Blackbird')  
		shipClass: (EveShipClass findByName: 'Cruiser');
		techLevel: EveTechLevel t1;
		race: EveRace caldari;
		save. 
	(self name: 'Caracal')  
		shipClass: (EveShipClass findByName: 'Cruiser');
		techLevel: EveTechLevel t1;
		race: EveRace caldari;
		save. 
	(self name: 'Moa')  
		shipClass: (EveShipClass findByName: 'Cruiser');
		techLevel: EveTechLevel t1;
		race: EveRace caldari;
		save. 
	(self name: 'Osprey')  
		shipClass: (EveShipClass findByName: 'Cruiser');
		techLevel: EveTechLevel t1;
		race: EveRace caldari;
		save. 

	(self name: 'Celestis')  
		shipClass: (EveShipClass findByName: 'Cruiser');
		techLevel: EveTechLevel t1;
		race: EveRace gallente;
		save. 
	(self name: 'Exequror')  
		shipClass: (EveShipClass findByName: 'Cruiser');
		techLevel: EveTechLevel t1;
		race: EveRace gallente;
		save. 
	(self name: 'Thorax')  
		shipClass: (EveShipClass findByName: 'Cruiser');
		techLevel: EveTechLevel t1;
		race: EveRace gallente;
		save. 
	(self name: 'Vexor')  
		shipClass: (EveShipClass findByName: 'Cruiser');
		techLevel: EveTechLevel t1;
		race: EveRace gallente;
		save. 

	(self name: 'Bellicose')  
		shipClass: (EveShipClass findByName: 'Cruiser');
		techLevel: EveTechLevel t1;
		race: EveRace minmatar;
		save. 
	(self name: 'Rupture')  
		shipClass: (EveShipClass findByName: 'Cruiser');
		techLevel: EveTechLevel t1;
		race: EveRace minmatar;
		save. 
	(self name: 'Scythe')  
		shipClass: (EveShipClass findByName: 'Cruiser');
		techLevel: EveTechLevel t1;
		race: EveRace minmatar;
		save. 
	(self name: 'Stabber')  
		shipClass: (EveShipClass findByName: 'Cruiser');
		techLevel: EveTechLevel t1;
		race: EveRace minmatar;
		save. 

as yet unclassified
initializeT1Destroyers
	"T1 - Destroyers"
	
	(self name: 'Coercer')  
		shipClass: (EveShipClass findByName: 'Destroyer');
		techLevel: EveTechLevel t1;
		race: EveRace amarr;
		save.	
	(self name: 'Dragoon')  
		shipClass: (EveShipClass findByName: 'Destroyer');
		techLevel: EveTechLevel t1;
		race: EveRace amarr;
		save.	
	(self name: 'Corax')  
		shipClass: (EveShipClass findByName: 'Destroyer');
		techLevel: EveTechLevel t1;
		race: EveRace caldari;
		save.	
	(self name: 'Cormorant')  
		shipClass: (EveShipClass findByName: 'Destroyer');
		techLevel: EveTechLevel t1;
		race: EveRace caldari;
		save.	
	(self name: 'Algos')  
		shipClass: (EveShipClass findByName: 'Destroyer');
		techLevel: EveTechLevel t1;
		race: EveRace gallente;		
		save.	
	(self name: 'Catalyst')  
		shipClass: (EveShipClass findByName: 'Destroyer');
		techLevel: EveTechLevel t1;
		race: EveRace gallente;		
		save.	
	(self name: 'Talwar')  
		shipClass: (EveShipClass findByName: 'Destroyer');
		techLevel: EveTechLevel t1;
		race: EveRace minmatar;		
		save.	
	(self name: 'Thrasher')  
		shipClass: (EveShipClass findByName: 'Destroyer');
		techLevel: EveTechLevel t1;
		race: EveRace minmatar;		
		save.	
as yet unclassified
initializeT1Dreadnoughts
	"T1 - Dreadnoughts"

	(self name: 'Revelation')  
		shipClass: (EveShipClass findByName: 'Dreadnought');
		techLevel: EveTechLevel t1;
		race: EveRace amarr;
		save. 
	(self name: 'Phoenix')  
		shipClass: (EveShipClass findByName: 'Dreadnought');
		techLevel: EveTechLevel t1;
		race: EveRace caldari;
		save. 
	(self name: 'Moros')  
		shipClass: (EveShipClass findByName: 'Dreadnought');
		techLevel: EveTechLevel t1;
		race: EveRace gallente;		
		save. 
	(self name: 'Naglfar')  
		shipClass: (EveShipClass findByName: 'Dreadnought');
		techLevel: EveTechLevel t1;
		race: EveRace minmatar;		
		save. 
as yet unclassified
initializeT1Freighters
	"T1 - Freighters"

	(self name: 'Providence')  
		shipClass: (EveShipClass findByName: 'Freighter');
		techLevel: EveTechLevel t1;
		race: EveRace amarr;
		save. 
	(self name: 'Charon')  
		shipClass: (EveShipClass findByName: 'Freighter');
		techLevel: EveTechLevel t1;
		race: EveRace caldari;
		save. 
	(self name: 'Obelisk')  
		shipClass: (EveShipClass findByName: 'Freighter');
		techLevel: EveTechLevel t1;
		race: EveRace gallente;		
		save. 
	(self name: 'Fenrir')  
		shipClass: (EveShipClass findByName: 'Freighter');
		techLevel: EveTechLevel t1;
		race: EveRace minmatar;		
		save. 

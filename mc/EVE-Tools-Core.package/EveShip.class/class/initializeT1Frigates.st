as yet unclassified
initializeT1Frigates
	"T1 - Frigates"
	
	"Amarr"
	(self name: 'Tormentor')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel t1;
		race: EveRace amarr;
		save.	
	(self name: 'Executioner')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel t1;
		race: EveRace amarr;
		save.	
	(self name: 'Crucifier')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel t1;
		race: EveRace amarr;
		save.	
	(self name: 'Punisher')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel t1;
		race: EveRace amarr;
		save.	
	(self name: 'Inquisitor')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel t1;
		race: EveRace amarr;
		save.	
	"Caldari"
	(self name: 'Bantam')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel t1;
		race: EveRace caldari;
		save.	
	(self name: 'Condor')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel t1;
		race: EveRace caldari;
		save.	
	(self name: 'Griffin')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel t1;
		race: EveRace caldari;
		save.	
	(self name: 'Heron')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel t1;
		race: EveRace caldari;
		save.	
	(self name: 'Merlin')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel t1;
		race: EveRace caldari;
		save.	
	(self name: 'Kestrel')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel t1;
		race: EveRace caldari;
		save.	
	"Galente"
	(self name: 'Navitas')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel t1;
		race: EveRace gallente;		
		save.	
	(self name: 'Atron')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel t1;
		race: EveRace gallente;		
		save.	
	(self name: 'Maulus')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel t1;
		race: EveRace gallente;		
		save.	
	(self name: 'Imicus')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel t1;
		race: EveRace gallente;		
		save.	
	(self name: 'Incursus')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel t1;
		race: EveRace gallente;		
		save.	
	(self name: 'Tristan')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel t1;
		race: EveRace gallente;		
		save.
	"Minmatar"	
	(self name: 'Burst')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel t1;
		race: EveRace minmatar;		
		save.	
	(self name: 'Slasher')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel t1;
		race: EveRace minmatar;		
		save.	
	(self name: 'Vigil')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel t1;
		race: EveRace minmatar;		
		save.	
	(self name: 'Probe')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel t1;
		race: EveRace minmatar;		
		save.	
	(self name: 'Rifter')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel t1;
		race: EveRace minmatar;		
		save.	
	(self name: 'Breacher')  
		shipClass: (EveShipClass findByName: 'Frigate');
		techLevel: EveTechLevel t1;
		race: EveRace minmatar;		
		save.
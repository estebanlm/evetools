as yet unclassified
initializeT1Industrials
	"T1 - Industrial"

	(self name: 'Bestower')  
		shipClass: (EveShipClass findByName: 'Industrial');
		techLevel: EveTechLevel t1;
		race: EveRace amarr;
		save. 
	(self name: 'Sigil')  
		shipClass: (EveShipClass findByName: 'Industrial');
		techLevel: EveTechLevel t1;
		race: EveRace amarr;
		save. 
	(self name: 'Badger')  
		shipClass: (EveShipClass findByName: 'Industrial');
		techLevel: EveTechLevel t1;
		race: EveRace caldari;
		save. 
	(self name: 'Badger mark II')  
		shipClass: (EveShipClass findByName: 'Industrial');
		techLevel: EveTechLevel t1;
		race: EveRace caldari;
		save. 
	(self name: 'Iteron mark I')  
		shipClass: (EveShipClass findByName: 'Industrial');
		techLevel: EveTechLevel t1;
		race: EveRace gallente;		
		save. 
	(self name: 'Iteron mark II')  
		shipClass: (EveShipClass findByName: 'Industrial');
		techLevel: EveTechLevel t1;
		race: EveRace gallente;		
		save. 
	(self name: 'Iteron mark III')  
		shipClass: (EveShipClass findByName: 'Industrial');
		techLevel: EveTechLevel t1;
		race: EveRace gallente;		
		save. 
	(self name: 'Iteron mark IV')  
		shipClass: (EveShipClass findByName: 'Industrial');
		techLevel: EveTechLevel t1;
		race: EveRace gallente;		
		save. 
	(self name: 'Iteron mark V')  
		shipClass: (EveShipClass findByName: 'Industrial');
		techLevel: EveTechLevel t1;
		race: EveRace gallente;		
		save. 
	(self name: 'Hoarder')  
		shipClass: (EveShipClass findByName: 'Industrial');
		techLevel: EveTechLevel t1;
		race: EveRace minmatar;		
		save. 
	(self name: 'Mammoth')  
		shipClass: (EveShipClass findByName: 'Industrial');
		techLevel: EveTechLevel t1;
		race: EveRace minmatar;		
		save. 
	(self name: 'Wreathe')  
		shipClass: (EveShipClass findByName: 'Industrial');
		techLevel: EveTechLevel t1;
		race: EveRace minmatar;		
		save. 
as yet unclassified
initializeT1JumpFreighters
	"T1 - Jump Freighters"

	(self name: 'Ark')  
		shipClass: (EveShipClass findByName: 'Jump Freighter');
		techLevel: EveTechLevel t1;
		race: EveRace amarr;
		save. 
	(self name: 'Rhea')  
		shipClass: (EveShipClass findByName: 'Jump Freighter');
		techLevel: EveTechLevel t1;
		race: EveRace caldari;
		save. 
	(self name: 'Anshar')  
		shipClass: (EveShipClass findByName: 'Jump Freighter');
		techLevel: EveTechLevel t1;
		race: EveRace gallente;		
		save. 
	(self name: 'Nomad')  
		shipClass: (EveShipClass findByName: 'Jump Freighter');
		techLevel: EveTechLevel t1;
		race: EveRace minmatar;		
		save. 
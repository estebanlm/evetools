as yet unclassified
initializeT1Rookie
	"T1 - Rookie ships"

	(self name: 'Impairor')  
		shipClass: (EveShipClass findByName: 'Rookie');
		techLevel: EveTechLevel t1;
		race: EveRace amarr;
		save.
	(self name: 'Ibis')  
		shipClass: (EveShipClass findByName: 'Rookie');
		techLevel: EveTechLevel t1;
		race: EveRace caldari;
		save.	
	(self name: 'Velator')  
		shipClass: (EveShipClass findByName: 'Rookie');
		techLevel: EveTechLevel t1;
		race: EveRace gallente;		
		save.		
	(self name: 'Reaper')  
		shipClass: (EveShipClass findByName: 'Rookie');
		techLevel: EveTechLevel t1;
		race: EveRace minmatar;		
		save.	
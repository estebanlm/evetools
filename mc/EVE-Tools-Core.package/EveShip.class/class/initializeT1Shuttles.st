as yet unclassified
initializeT1Shuttles
	"T1 - Shuttles"

	(self name: 'Amarr shuttle')  
		shipClass: (EveShipClass findByName: 'Shuttle');
		techLevel: EveTechLevel t1;
		race: EveRace amarr;
		save. 
	(self name: 'Caldari shuttle')  
		shipClass: (EveShipClass findByName: 'Shuttle');
		techLevel: EveTechLevel t1;
		race: EveRace caldari;
		save. 
	(self name: 'Gallente shuttle')  
		shipClass: (EveShipClass findByName: 'Shuttle');
		techLevel: EveTechLevel t1;
		race: EveRace gallente;		
		save. 
	(self name: 'Minmatar shuttle')  
		shipClass: (EveShipClass findByName: 'Shuttle');
		techLevel: EveTechLevel t1;
		race: EveRace minmatar;		
		save. 
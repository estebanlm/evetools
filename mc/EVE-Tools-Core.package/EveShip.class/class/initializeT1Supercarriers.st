as yet unclassified
initializeT1Supercarriers
	"T1 - Supercarriers"

	(self name: 'Aeon')  
		shipClass: (EveShipClass findByName: 'Supercarrier');
		techLevel: EveTechLevel t1;
		race: EveRace amarr;
		save. 
	(self name: 'Wyvern')  
		shipClass: (EveShipClass findByName: 'Supercarrier');
		techLevel: EveTechLevel t1;
		race: EveRace caldari;
		save. 
	(self name: 'Nyx')  
		shipClass: (EveShipClass findByName: 'Supercarrier');
		techLevel: EveTechLevel t1;
		race: EveRace gallente;		
		save. 
	(self name: 'Hel')  
		shipClass: (EveShipClass findByName: 'Supercarrier');
		techLevel: EveTechLevel t1;
		race: EveRace minmatar;		
		save. 

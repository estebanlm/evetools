as yet unclassified
initializeT1Titan
	"T1 - Titan"

	(self name: 'Avatar')  
		shipClass: (EveShipClass findByName: 'Titan');
		techLevel: EveTechLevel t1;
		race: EveRace amarr;
		save. 
	(self name: 'Leviathan')  
		shipClass: (EveShipClass findByName: 'Titan');
		techLevel: EveTechLevel t1;
		race: EveRace caldari;
		save. 
	(self name: 'Erebus')  
		shipClass: (EveShipClass findByName: 'Titan');
		techLevel: EveTechLevel t1;
		race: EveRace gallente;		
		save. 
	(self name: 'Ragnarok')  
		shipClass: (EveShipClass findByName: 'Titan');
		techLevel: EveTechLevel t1;
		race: EveRace minmatar;		
		save. 

as yet unclassified
initializeT2AssaultShips
	"T2 - Assault ships"

	(self name: 'Retribution')  
		shipClass: (EveShipClass findByName: 'Assault Ship');
		techLevel: EveTechLevel t2;
		race: EveRace amarr;
		save. 
	(self name: 'Vengeance')  
		shipClass: (EveShipClass findByName: 'Assault Ship');
		techLevel: EveTechLevel t2;
		race: EveRace amarr;
		save. 
	(self name: 'Harpy')  
		shipClass: (EveShipClass findByName: 'Assault Ship');
		techLevel: EveTechLevel t2;
		race: EveRace caldari;
		save. 
	(self name: 'Hawk')  
		shipClass: (EveShipClass findByName: 'Assault Ship');
		techLevel: EveTechLevel t2;
		race: EveRace caldari;
		save. 
	(self name: 'Enyo')  
		shipClass: (EveShipClass findByName: 'Assault Ship');
		techLevel: EveTechLevel t2;
		race: EveRace gallente;		
		save. 
	(self name: 'Ishkur')  
		shipClass: (EveShipClass findByName: 'Assault Ship');
		techLevel: EveTechLevel t2;
		race: EveRace gallente;		
		save. 
	(self name: 'Jaguar')  
		shipClass: (EveShipClass findByName: 'Assault Ship');
		techLevel: EveTechLevel t2;
		race: EveRace minmatar;		
		save. 
	(self name: 'Wolf')  
		shipClass: (EveShipClass findByName: 'Assault Ship');
		techLevel: EveTechLevel t2;
		race: EveRace minmatar;		
		save. 
	
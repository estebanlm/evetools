as yet unclassified
initializeT2BlackOps
	"T2 - Black Ops"

	(self name: 'Redeemer')  
		shipClass: (EveShipClass findByName: 'Black Ops');
		techLevel: EveTechLevel t2;
		race: EveRace amarr;
		save. 
	(self name: 'Widow')  
		shipClass: (EveShipClass findByName: 'Black Ops');
		techLevel: EveTechLevel t2;
		race: EveRace caldari;
		save. 
	(self name: 'Sin')  
		shipClass: (EveShipClass findByName: 'Black Ops');
		techLevel: EveTechLevel t2;
		race: EveRace gallente;		
		save. 
	(self name: 'Panther')  
		shipClass: (EveShipClass findByName: 'Black Ops');
		techLevel: EveTechLevel t2;
		race: EveRace minmatar;		
		save.
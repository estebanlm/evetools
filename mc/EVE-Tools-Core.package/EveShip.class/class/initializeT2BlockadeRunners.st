as yet unclassified
initializeT2BlockadeRunners
	"T2 - Blockade Runners"

	(self name: 'Prorator')  
		shipClass: (EveShipClass findByName: 'Blockade Runner');
		techLevel: EveTechLevel t2;
		race: EveRace amarr;
		save. 
	(self name: 'Crane')  
		shipClass: (EveShipClass findByName: 'Blockade Runner');
		techLevel: EveTechLevel t2;
		race: EveRace caldari;
		save. 
	(self name: 'Viator')  
		shipClass: (EveShipClass findByName: 'Blockade Runner');
		techLevel: EveTechLevel t2;
		race: EveRace gallente;		
		save. 
	(self name: 'Prowler')  
		shipClass: (EveShipClass findByName: 'Blockade Runner');
		techLevel: EveTechLevel t2;
		race: EveRace minmatar;		
		save. 

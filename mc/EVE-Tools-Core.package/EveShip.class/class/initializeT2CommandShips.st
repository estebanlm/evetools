as yet unclassified
initializeT2CommandShips
	"T2 - Command"
	
	(self name: 'Absolution')  
		shipClass: (EveShipClass findByName: 'Command');
		techLevel: EveTechLevel t2;
		race: EveRace amarr;
		save. 
	(self name: 'Damnation')  
		shipClass: (EveShipClass findByName: 'Command');
		techLevel: EveTechLevel t2;
		race: EveRace amarr;
		save. 
	(self name: 'Nighthawk')  
		shipClass: (EveShipClass findByName: 'Command');
		techLevel: EveTechLevel t2;
		race: EveRace caldari;
		save. 
	(self name: 'Vulture')  
		shipClass: (EveShipClass findByName: 'Command');
		techLevel: EveTechLevel t2;
		race: EveRace caldari;
		save. 
	(self name: 'Astarte')  
		shipClass: (EveShipClass findByName: 'Command');
		techLevel: EveTechLevel t2;
		race: EveRace gallente;		
		save. 
	(self name: 'Eos')  
		shipClass: (EveShipClass findByName: 'Command');
		techLevel: EveTechLevel t2;
		race: EveRace gallente;		
		save. 
	(self name: 'Claymore')  
		shipClass: (EveShipClass findByName: 'Command');
		techLevel: EveTechLevel t2;
		race: EveRace minmatar;		
		save. 
	(self name: 'Sleipnir')  
		shipClass: (EveShipClass findByName: 'Command');
		techLevel: EveTechLevel t2;
		race: EveRace minmatar;		
		save. 
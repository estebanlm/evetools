as yet unclassified
initializeT2CoverOps
	"T2 - Cover Ops"
	
	(self name: 'Anathema')  
		shipClass: (EveShipClass findByName: 'Cover Ops');
		techLevel: EveTechLevel t2;
		race: EveRace amarr;
		save. 
	(self name: 'Purifier')  
		shipClass: (EveShipClass findByName: 'Cover Ops');
		techLevel: EveTechLevel t2;
		race: EveRace amarr;
		save. 
	(self name: 'Buzzard')  
		shipClass: (EveShipClass findByName: 'Cover Ops');
		techLevel: EveTechLevel t2;
		race: EveRace caldari;
		save. 
	(self name: 'Manticore')  
		shipClass: (EveShipClass findByName: 'Cover Ops');
		techLevel: EveTechLevel t2;
		race: EveRace caldari;
		save. 
	(self name: 'Helios')  
		shipClass: (EveShipClass findByName: 'Cover Ops');
		techLevel: EveTechLevel t2;
		race: EveRace gallente;		
		save. 
	(self name: 'Nemesis')  
		shipClass: (EveShipClass findByName: 'Cover Ops');
		techLevel: EveTechLevel t2;
		race: EveRace gallente;		
		save. 
	(self name: 'Cheetah')  
		shipClass: (EveShipClass findByName: 'Cover Ops');
		techLevel: EveTechLevel t2;
		race: EveRace minmatar;		
		save. 
	(self name: 'Hound')  
		shipClass: (EveShipClass findByName: 'Cover Ops');
		techLevel: EveTechLevel t2;
		race: EveRace minmatar;		
		save. 
	
as yet unclassified
initializeT2ElectonicAttackShips
	"T2 - Electronic Attack Ships"
	
	(self name: 'Sentinel')  
		shipClass: (EveShipClass findByName: 'Electronic Attack Ship');
		techLevel: EveTechLevel t2;
		race: EveRace amarr;
		save. 
	(self name: 'Kitsune')  
		shipClass: (EveShipClass findByName: 'Electronic Attack Ship');
		techLevel: EveTechLevel t2;
		race: EveRace caldari;
		save. 
	(self name: 'Keres')  
		shipClass: (EveShipClass findByName: 'Electronic Attack Ship');
		techLevel: EveTechLevel t2;
		race: EveRace gallente;		
		save. 
	(self name: 'Hyena')  
		shipClass: (EveShipClass findByName: 'Electronic Attack Ship');
		techLevel: EveTechLevel t2;
		race: EveRace minmatar;		
		save. 

as yet unclassified
initializeT2HeavyAssaultShips
	"T2 - Heavy Assault ships"

	(self name: 'Sacrilege')  
		shipClass: (EveShipClass findByName: 'Heavy Assault Ship');
		techLevel: EveTechLevel t2;
		race: EveRace amarr;
		save. 
	(self name: 'Zealot')  
		shipClass: (EveShipClass findByName: 'Heavy Assault Ship');
		techLevel: EveTechLevel t2;
		race: EveRace amarr;
		save. 
	(self name: 'Cerberus')  
		shipClass: (EveShipClass findByName: 'Heavy Assault Ship');
		techLevel: EveTechLevel t2;
		race: EveRace caldari;
		save. 
	(self name: 'Eagle')  
		shipClass: (EveShipClass findByName: 'Heavy Assault Ship');
		techLevel: EveTechLevel t2;
		race: EveRace caldari;
		save. 
	(self name: 'Deimos')  
		shipClass: (EveShipClass findByName: 'Heavy Assault Ship');
		techLevel: EveTechLevel t2;
		race: EveRace gallente;		
		save. 
	(self name: 'Ishtar')  
		shipClass: (EveShipClass findByName: 'Heavy Assault Ship');
		techLevel: EveTechLevel t2;
		race: EveRace gallente;		
		save. 
	(self name: 'Muninn')  
		shipClass: (EveShipClass findByName: 'Heavy Assault Ship');
		techLevel: EveTechLevel t2;
		race: EveRace minmatar;		
		save. 
	(self name: 'Vagabond')  
		shipClass: (EveShipClass findByName: 'Heavy Assault Ship');
		techLevel: EveTechLevel t2;
		race: EveRace minmatar;		
		save. 

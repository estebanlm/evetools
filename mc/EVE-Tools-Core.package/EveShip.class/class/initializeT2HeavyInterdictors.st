as yet unclassified
initializeT2HeavyInterdictors
	"T2 - Heavy Interdictors"

	(self name: 'Devoter')  
		shipClass: (EveShipClass findByName: 'Heavy Interdictor');
		techLevel: EveTechLevel t2;
		race: EveRace amarr;
		save. 
	(self name: 'Onyx')  
		shipClass: (EveShipClass findByName: 'Heavy Interdictor');
		techLevel: EveTechLevel t2;
		race: EveRace caldari;
		save. 
	(self name: 'Phobos')  
		shipClass: (EveShipClass findByName: 'Heavy Interdictor');
		techLevel: EveTechLevel t2;
		race: EveRace gallente;		
		save. 
	(self name: 'Broadsword')  
		shipClass: (EveShipClass findByName: 'Heavy Interdictor');
		techLevel: EveTechLevel t2;
		race: EveRace minmatar;		
		save. 

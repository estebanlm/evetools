as yet unclassified
initializeT2Interceptors
	"T2 - Interceptors"
	
	(self name: 'Crusader')  
		shipClass: (EveShipClass findByName: 'Interceptor');
		techLevel: EveTechLevel t2;
		race: EveRace amarr;
		save. 
	(self name: 'Malediction')  
		shipClass: (EveShipClass findByName: 'Interceptor');
		techLevel: EveTechLevel t2;
		race: EveRace amarr;
		save. 
	(self name: 'Crow')  
		shipClass: (EveShipClass findByName: 'Interceptor');
		techLevel: EveTechLevel t2;
		race: EveRace caldari;
		save. 
	(self name: 'Raptor')  
		shipClass: (EveShipClass findByName: 'Interceptor');
		techLevel: EveTechLevel t2;
		race: EveRace caldari;
		save. 
	(self name: 'Ares')  
		shipClass: (EveShipClass findByName: 'Interceptor');
		techLevel: EveTechLevel t2;
		race: EveRace gallente;		
		save. 
	(self name: 'Taranis')  
		shipClass: (EveShipClass findByName: 'Interceptor');
		techLevel: EveTechLevel t2;
		race: EveRace gallente;		
		save. 
	(self name: 'Claw')  
		shipClass: (EveShipClass findByName: 'Interceptor');
		techLevel: EveTechLevel t2;
		race: EveRace minmatar;		
		save. 
	(self name: 'Stiletto')  
		shipClass: (EveShipClass findByName: 'Interceptor');
		techLevel: EveTechLevel t2;
		race: EveRace minmatar;		
		save.
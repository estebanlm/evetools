as yet unclassified
initializeT2Interdictors
	"T2 - Interdictors"
	
	(self name: 'Heretic')  
		shipClass: (EveShipClass findByName: 'Interdictor');
		techLevel: EveTechLevel t2;
		race: EveRace amarr;
		save. 
	(self name: 'Flycatcher')  
		shipClass: (EveShipClass findByName: 'Interdictor');
		techLevel: EveTechLevel t2;
		race: EveRace caldari;
		save. 
	(self name: 'Eris')  
		shipClass: (EveShipClass findByName: 'Interdictor');
		techLevel: EveTechLevel t2;
		race: EveRace gallente;		
		save. 
	(self name: 'Sabre')  
		shipClass: (EveShipClass findByName: 'Interdictor');
		techLevel: EveTechLevel t2;
		race: EveRace minmatar;		
		save. 
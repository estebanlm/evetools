as yet unclassified
initializeT2Logistics
	"T2 - Logistics"

	(self name: 'Guardian')  
		shipClass: (EveShipClass findByName: 'Logistics');
		techLevel: EveTechLevel t2;
		race: EveRace amarr;
		save. 
	(self name: 'Basilisk')  
		shipClass: (EveShipClass findByName: 'Logistics');
		techLevel: EveTechLevel t2;
		race: EveRace caldari;
		save. 
	(self name: 'Oneiros')  
		shipClass: (EveShipClass findByName: 'Logistics');
		techLevel: EveTechLevel t2;
		race: EveRace gallente;		
		save. 
	(self name: 'Scimitar')  
		shipClass: (EveShipClass findByName: 'Logistics');
		techLevel: EveTechLevel t2;
		race: EveRace minmatar;		
		save. 

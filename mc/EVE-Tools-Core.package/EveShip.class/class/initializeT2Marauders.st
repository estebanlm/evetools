as yet unclassified
initializeT2Marauders
	"T2 - Marauders"
	
	(self name: 'Paladin')  
		shipClass: (EveShipClass findByName: 'Marauder');
		techLevel: EveTechLevel t2;
		race: EveRace amarr;
		save. 
	(self name: 'Golem')  
		shipClass: (EveShipClass findByName: 'Marauder');
		techLevel: EveTechLevel t2;
		race: EveRace caldari;
		save. 
	(self name: 'Kronos')  
		shipClass: (EveShipClass findByName: 'Marauder');
		techLevel: EveTechLevel t2;
		race: EveRace gallente;		
		save. 
	(self name: 'Vargur')  
		shipClass: (EveShipClass findByName: 'Marauder');
		techLevel: EveTechLevel t2;
		race: EveRace minmatar;		
		save. 
	
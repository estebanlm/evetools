as yet unclassified
initializeT2ReconShips
	"T2 - Recon"
	
	(self name: 'Curse')  
		shipClass: (EveShipClass findByName: 'Recon');
		techLevel: EveTechLevel t2;
		race: EveRace amarr;
		save. 
	(self name: 'Pilgrim')  
		shipClass: (EveShipClass findByName: 'Recon');
		techLevel: EveTechLevel t2;
		race: EveRace amarr;
		save. 
	(self name: 'Falcon')  
		shipClass: (EveShipClass findByName: 'Recon');
		techLevel: EveTechLevel t2;
		race: EveRace caldari;
		save. 
	(self name: 'Rook')  
		shipClass: (EveShipClass findByName: 'Recon');
		techLevel: EveTechLevel t2;
		race: EveRace caldari;
		save. 
	(self name: 'Arazu')  
		shipClass: (EveShipClass findByName: 'Recon');
		techLevel: EveTechLevel t2;
		race: EveRace gallente;		
		save. 
	(self name: 'Lachesis')  
		shipClass: (EveShipClass findByName: 'Recon');
		techLevel: EveTechLevel t2;
		race: EveRace gallente;		
		save. 
	(self name: 'Huginn')  
		shipClass: (EveShipClass findByName: 'Recon');
		techLevel: EveTechLevel t2;
		race: EveRace minmatar;		
		save. 
	(self name: 'Rapier')  
		shipClass: (EveShipClass findByName: 'Recon');
		techLevel: EveTechLevel t2;
		race: EveRace minmatar;		
		save. 

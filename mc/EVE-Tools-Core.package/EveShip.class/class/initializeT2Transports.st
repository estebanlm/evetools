as yet unclassified
initializeT2Transports
	"T2 - Transports"

	(self name: 'Impel')  
		shipClass: (EveShipClass findByName: 'Transport');
		techLevel: EveTechLevel t2;
		race: EveRace amarr;
		save. 
	(self name: 'Bustard')  
		shipClass: (EveShipClass findByName: 'Transport');
		techLevel: EveTechLevel t2;
		race: EveRace caldari;
		save. 
	(self name: 'Occator')  
		shipClass: (EveShipClass findByName: 'Transport');
		techLevel: EveTechLevel t2;
		race: EveRace gallente;		
		save. 
	(self name: 'Mastodon')  
		shipClass: (EveShipClass findByName: 'Transport');
		techLevel: EveTechLevel t2;
		race: EveRace minmatar;		
		save.
as yet unclassified
initializeT3StrategicCruiser
	"T3 - Strategic cruiser"
	
	(self name: 'Legion')  
		shipClass: (EveShipClass findByName: 'Strategic Cruiser');
		techLevel: EveTechLevel t3;
		race: EveRace amarr;
		save. 
	(self name: 'Tengu')  
		shipClass: (EveShipClass findByName: 'Strategic Cruiser');
		techLevel: EveTechLevel t3;
		race: EveRace caldari;
		save. 
	(self name: 'Proteus')  
		shipClass: (EveShipClass findByName: 'Strategic Cruiser');
		techLevel: EveTechLevel t3;
		race: EveRace gallente;		
		save. 
	(self name: 'Loki')  
		shipClass: (EveShipClass findByName: 'Strategic Cruiser');
		techLevel: EveTechLevel t3;
		race: EveRace minmatar;		
		save. 

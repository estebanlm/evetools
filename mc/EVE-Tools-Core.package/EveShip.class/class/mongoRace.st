persistence
mongoRace
	<mongoDescription>
	
	^ VOMongoToOneDescription new
		attributeName: 'race';
		accessor: (MAPluggableAccessor 
			read: [ :ship | ship race name ]
			write: [ :ship :value | ship race: (EveRace findByName: value) ]);
		yourself
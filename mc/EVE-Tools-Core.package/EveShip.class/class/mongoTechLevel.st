persistence
mongoTechLevel
	<mongoDescription>
	
	^ VOMongoToOneDescription new
		attributeName: 'techLevel';
		accessor: (MAPluggableAccessor 
			read: [ :ship | ship techLevel name ]
			write: [ :ship :value | ship techLevel: (EveTechLevel findByName: value) ]);
		yourself
as yet unclassified
initializeShipClasses

	(self name: 'Capsule') save.
	(self name: 'Shuttle') save.
	(self name: 'Rookie') save.
	(self name: 'Frigate') save.		
	(self name: 'Destroyer') save.		
	(self name: 'Cruiser') save.		
	(self name: 'Combat Battlecruiser') save.		
	(self name: 'Battleship') save.		
	(self name: 'Assault Ship') save.
	(self name: 'Marauder') save.		
	(self name: 'Black Ops') save.		
	(self name: 'Interceptor') save.		
	(self name: 'Interdictor') save.		
	(self name: 'Heavy Interdictor') save.		
	(self name: 'Heavy Assault Ship') save.
	(self name: 'Recon') save.		
	(self name: 'Command') save.		
	(self name: 'Strategic Cruiser') save.		
	(self name: 'Assault Battlecruiser') save.		
	(self name: 'Carrier') save.		
	(self name: 'Dreadnought') save.		
	(self name: 'Supercarrier') save.		
	(self name: 'Titan') save.		
	(self name: 'Industrial') save.		
	(self name: 'Freighter') save.		
	(self name: 'Jump Freighter') save.		
	(self name: 'Transport') save.		
	(self name: 'Blockade Runner') save.		
	(self name: 'Logistics') save.		
	(self name: 'Mining Barge') save.
	(self name: 'Exhumer') save.
	(self name: 'Capital Industrial') save.
	(self name: 'Electronic Attack Ship') save.
	(self name: 'Cover Ops') save.
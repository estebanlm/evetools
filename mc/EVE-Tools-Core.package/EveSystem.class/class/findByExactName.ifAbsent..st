accessing
findByExactName: aString ifAbsent: absentBlock 
	^ (self selectOne: { #name -> aString } asDictionary)
		ifNil: absentBlock

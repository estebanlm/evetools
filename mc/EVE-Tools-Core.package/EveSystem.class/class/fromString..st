instance creation
fromString: aString
	^ self basicNew 
		initializeFromString: aString;
		yourself
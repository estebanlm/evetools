class initialization
importSystemsFrom: aReference 
	aReference readStreamDo: [ :stream |
		stream ascii.
		(stream upTo: Character lf) asString. "Skip first (it is the header)"
		[ stream atEnd ] whileFalse: [ 
			(self fromString: (stream upTo: Character lf)) save ] ]
accessing
unknown
	^self voyageRepository
		secundaryCacheAt: #SystemUnknown 
		ifAbsentPut: [ self selectOne: [ :each | each name = 'Unknown' ] asMongoQuery ]
initialization
initializeFromString: aString 
	| parsed |
	
	self initialize.
	
	parsed := aString subStrings: ',"'.
	
	regionId := (parsed at: 1) asNumber.
	constelationId := (parsed at: 2) asNumber.
	id := (parsed at: 3) asNumber.
	self name: (parsed at: 4).
	security := (parsed at: 22) asNumber.
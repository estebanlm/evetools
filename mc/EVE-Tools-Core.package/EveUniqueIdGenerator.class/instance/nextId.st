as yet unclassified
nextId
	| chars stream seed size |

	chars := self characters. 
	size := chars size.

	seed := Time millisecondClockValue * 1000000.
	stream := String new writeStream.
	1 to: self size do: [ :ignored | | pos |
		[ pos := seed atRandom % size.
	  	pos > 0 ] whileFalse. 
		stream nextPut: (chars at: pos) ].
	
	^ stream contents

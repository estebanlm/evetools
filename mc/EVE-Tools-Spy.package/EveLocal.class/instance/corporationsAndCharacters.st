accessing
corporationsAndCharacters
	"Answer characters grouped by corp"
	| result |
	
	result := Dictionary new.
	self characters do: [ :each |
		(result 
			at: each corporation
			ifAbsentPut: [ Set new ])
			add: each ].
		
	^ result
initialization
initializeFromString: aString
	characters := (aString trimBoth subStrings: String cr) 
		collect: [ :each | | pilotName |
			pilotName := each trimBoth.
			EveCharacter 
				findByName: pilotName
				ifAbsent: [ (EveCharacter name: pilotName) save ] ].

executing
go 
	| repository |
	
	repository := VORepository current. 
	[ [ VOCurrentRepository 
			use: repository 
			during: [ self updateCharacters ] ] 
		ensure: [ finish := true ] ] fork
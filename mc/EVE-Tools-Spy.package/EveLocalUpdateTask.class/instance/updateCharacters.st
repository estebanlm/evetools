executing
updateCharacters
	[ 
	  (self characters 
			select: #needUpdate)
			do: [ :each |
				self updateCharacter: each.
				updated := updated + 1 ] ]
	on: Error do: [ :e | self finishWithError: e ]
private
updateCharactersIds: aCollection 
	| xml |
	
	xml := EveConnect new characterIdsFor: (aCollection collect: #name).
	aCollection do: [ :each | each updateIdFrom: xml ]
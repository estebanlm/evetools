testing
includesShip: aShip
	^self ships anySatisfy: [ :each |
		each matchWith: aShip ]
initialization
initializeFromString: aString
	objects := ((aString trimBoth subStrings: String cr) 
		collect: [ :each | EveScanObject fromString: each ])
		select: #isShip.
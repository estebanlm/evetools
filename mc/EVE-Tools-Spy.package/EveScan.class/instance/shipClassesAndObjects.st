accessing
shipClassesAndObjects
	^ self ships groupedBy: [ :each | each type shipClass ]

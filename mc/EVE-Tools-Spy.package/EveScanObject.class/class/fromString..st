instance creation
fromString: aString
	^self basicNew
		initialize: aString;
		yourself
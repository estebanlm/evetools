initialization
initialize: aString 
	| data |
	
	self initialize.
	
	data := (aString subStrings: Character tab asString) collect: #trimBoth.
	name := data first.
	type := EveObject 
		findByName: data second 
		ifAbsent: [ EveUnknownObject name: data second  ].
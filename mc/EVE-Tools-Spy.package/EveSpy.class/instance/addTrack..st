accessing
addTrack: aTrack
	self tracks add: aTrack.
	self 
		analysisAt: aTrack  
		put: (EveSpyAnalysis spy: self track: aTrack)
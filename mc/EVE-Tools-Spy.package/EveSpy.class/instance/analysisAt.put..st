private
analysisAt: aTrack put: anAnalysis 	
	^ self analysis 
		at: aTrack 
		put: anAnalysis
accessing
toggleMute: anObject
	self muted
		remove: anObject
		ifAbsent: [ self muted add: anObject ]
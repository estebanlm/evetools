instance creation
spy: aSpy track: aTrack 
	^ self basicNew 
		initializeSpy: aSpy track: aTrack;
		yourself
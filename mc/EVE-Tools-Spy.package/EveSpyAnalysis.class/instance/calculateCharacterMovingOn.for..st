initialization
calculateCharacterMovingOn: spy for: track
	"Make an analysis of the characters that are on the move. 
	 That means, answers the ones that repeates in other systems"
	| tracks last rest characters |
	
	tracks := spy tracks copyUpThrough: track.
	tracks size <= 1 ifTrue: [ ^ Dictionary new ].
	
	last := tracks last.
	rest := tracks allButLast.
	[ rest notEmpty and: [ rest last system = last system ] ]
		whileTrue: [ rest := rest allButLast ].
	
	characters := Dictionary new.
	(last local characters 
		reject: [ :each | (spy isMuted: each) or: [ spy isMuted: each corporation ]  ])
		do: [ :pilot | | presentTracks |
			presentTracks := rest select: [ :eachTrack | 
				(eachTrack system ~= last system)
					and: [ eachTrack includesCharacter: pilot ] ]. 
		
		presentTracks ifNotEmpty: [ characters at: pilot put: presentTracks ] ].
	
	^characters.
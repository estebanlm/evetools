initialization
calculateCharacterStaticNewOn: spy for: track 
	"Makes an analysis of the characters that appeared in the system. 
	 That means, answers the ones that are new in the system"
	| last previous  tracks |
	
	tracks := spy tracks copyUpThrough: track.
	tracks size <= 1 ifTrue: [ ^ Dictionary new ].

	last := tracks last.	
	previous := tracks before: last.
	last system = previous system 
		ifFalse: [ ^ Dictionary new ].
	
	^ last local characters 
		reject: [ :each | 
			(previous includesCharacter: each) 
				or: [ 
					(spy isMuted: each)
					or: [ spy isMuted: each corporation ] ] ].
initialization
calculateShipMovementOn: spy for: track
	"Makes an analysis of the ships that are on the move. 
	 That means, answers the ones that repeates in other systems"
	| last rest ships tracks |
	
	tracks := spy tracks copyUpThrough: track.
	tracks size <= 1 ifTrue: [ ^ Dictionary new ].

	last := tracks last.	
	rest := tracks allButLast.
	[ rest notEmpty and: [ rest last system = last system ] ]
		whileTrue: [ rest := rest allButLast ].
	
	ships := Dictionary new.
	(last scan ships 
		reject: [ :each | spy isMuted: each ])
		do: [ :ship | | presentTracks |
		presentTracks := rest select: [ :eachTrack | 
			(eachTrack system ~= last system)
				and: [ eachTrack includesShip: ship ] ]. 
		
		presentTracks ifNotEmpty: [ 
			ships at: ship put: presentTracks ] ].
	
	^ships.
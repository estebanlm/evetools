initialization
calculateShipStaticNewOn: spy for: track
	"Makes an analysis of the ships that appeared in the system. 
	 That means, answers the ones that are new in the system"
	| last previous  tracks |
	
	tracks := spy tracks copyUpThrough: track.
	tracks size <= 1 ifTrue: [ ^ Dictionary new ].

	last := tracks last.	
	previous := tracks before: last.
	last system = previous system 
		ifFalse: [ ^ Dictionary new ].
	
	^ last scan ships 
		reject: [ :each | 
			(previous includesShip: each)
				or: [ spy isMuted: each  ] ].
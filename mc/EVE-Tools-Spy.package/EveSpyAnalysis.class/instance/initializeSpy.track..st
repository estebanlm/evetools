initialization
initializeSpy: aSpy track: aTrack
	self initialize.
	
	charactersMoving := self calculateCharacterMovingOn: aSpy for: aTrack.
	charactersStaticNew := self calculateCharacterStaticNewOn: aSpy for: aTrack.
	shipsMoving := self calculateShipMovementOn: aSpy for: aTrack.
	shipsStaticNew := self calculateShipStaticNewOn: aSpy for: aTrack.
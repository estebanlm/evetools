showing
doShow: aPart
	super doShow: aPart.
	aPart 
		on: EveTrackAdded do: [ :ann | self header refreshHistory ];
		on: EveSpyRestarted do: [ :ann | self restartSpy ].
	
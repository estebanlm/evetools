instance creation
for: aCharacter
	^self basicNew
		initialize: aCharacter;
		yourself
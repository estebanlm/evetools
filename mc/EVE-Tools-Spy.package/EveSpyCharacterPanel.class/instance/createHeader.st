private
createHeader
	^ REContainer new 
		add: (REImage url: self character urlPortrait64);
		add: self character name;
		yourself
private
createReportGridFor: aCollection
	| grid |
	
	grid := RESimpleGrid new
		columns: 3;
		yourself.
	
	aCollection do: [ :eachTrack | 
		grid 
			add: eachTrack date asString; 
			add: eachTrack system asString; 
			add: (REAnchor new 
				label: 'See';
				url: (self urlFor: eachTrack);
				yourself) ].

	^ grid
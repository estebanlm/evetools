accessing
path
	^ self class path copyWith: (self character id asString)
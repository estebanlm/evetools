private
createHistoryPanel
	| tracks tracksInMenu tracksToShow |
	
	tracks := self tracks collect:[ :each |
		REAnchor new
			label: each systemName;
			callback: [ self showTrack: each ];
			yourself ]. 
			
	tracks size > self historyToShow
		ifTrue: [ 
			tracksInMenu := (tracks allButLast: self historyToShow) reversed.
			tracksToShow := tracks last: self historyToShow ]
		ifFalse: [
			tracksToShow := tracks ].
	
	^ RESimpleBreadcrumb new 
		add: [ 
			RESimpleDropdown new 
				title: '...';
				addAll: tracksInMenu;
				yourself ] 
			if: tracksInMenu notNil;
		addAll: tracksToShow;
		yourself
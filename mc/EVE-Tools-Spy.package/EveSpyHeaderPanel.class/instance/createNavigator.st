private
createNavigator
	^ RESimpleNavigationBar new
		addBefore: (REAnchor new 
			class: 'brand';
			label: EveSpyApplication applicationTitle;
			yourself);
		add: (REAnchor new 
			label: 'New Track';
			url: (self basePathWith: 'new');
			yourself);
		add: (REAnchor new 
			label: 'History';
			url: (self basePathWith: 'track-history');
			yourself);
		"add: (REAnchor new 
			label: 'Knowledge Base';
			callback: [ self showKnowledgeBase ];
			yourself);"
		add: (REAnchor new 
			label: 'About';
			url: (self basePathWith: 'about');
			yourself);
		addAfter: self createSearch;
		yourself
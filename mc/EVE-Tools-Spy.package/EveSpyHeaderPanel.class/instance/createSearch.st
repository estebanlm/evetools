private
createSearch
	| searchResult |

	^REForm new 
		class: 'navbar-search pull-right';
		add: (RETextField new
			example: 'Search system or pilot';
			class: 'search-query';
			addDecoration: (REAutocompleteDecoration new 
				labels: [ :v | v nameWithLabel ];
				search: [ :v | self resolveSearch: v trimBoth ];
				yourself);
			onEnter: [ self showSearchResult: searchResult ];
			callback: [ :v | searchResult := v ];
			yourself);
		yourself
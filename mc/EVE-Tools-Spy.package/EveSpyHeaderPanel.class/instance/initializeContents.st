initialization
initializeContents 
	self class: 'row'.
	self class: 'header'.
	
	self add: self createNavigator. 
	self add: (historyPanel := RESimpleRow new).
	
	self spy hasManyTracks 
		ifTrue: [ historyPanel add: self createHistoryPanel ]
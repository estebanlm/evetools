accessing
refreshHistory
	historyPanel removeAll.
	self spy hasManyTracks 
		ifTrue: [ 
			historyPanel  
				add: self createHistoryPanel;
				refresh ]
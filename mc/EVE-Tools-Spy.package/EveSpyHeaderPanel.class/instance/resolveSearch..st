private
resolveSearch: aString
 	^aString size > 1 
		ifTrue: [ 
			OrderedCollection new
				addAll: (EveCharacter findByNameMatching: aString limit: EveSpyApplication searchLimit);
				addAll: (EveSystem findByNameMatching: aString limit: EveSpyApplication searchLimit);
				yourself ]
		ifFalse: [ 
			#() ]
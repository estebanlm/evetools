private
showSearchResult: aStringOrObject
	| eveObjectToShow |
	
	eveObjectToShow := aStringOrObject isString
		ifTrue: [  
			| resolved |
			resolved := self resolveSearch: aStringOrObject trimBoth.
			resolved size = 1
				ifTrue: [ resolved first ]
				ifFalse: [ nil ] ]
		ifFalse: [ 
			aStringOrObject  ].
		
	eveObjectToShow ifNotNil: [ 
		self showGeneric: eveObjectToShow ]
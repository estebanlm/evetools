style
styleLess
^ '
@linkColor: rgb(51, 181, 229);
@buttonActiveBackgroundColor: #2e2e2e; //#4d4d4d;
@buttonActiveColor: #000000;
 
body { 
	font-size: 0.8em;
}

a {
	cursor: pointer;
	//color:#FF961F;
}

div[class*="span"] {
	margin-left: 10px;
	margin-right: 10px;
}

.warning {
	font-size: 0.9em;
	font-style: italic;
}

.container {
	table { 
		th {  
			padding: 3px; 
		}

		td {  
			padding: 3px; 
		}
	}
	
	.alert { 
		padding-top: 3px;
		padding-bottom: 3px;
	} 
}

.btn.active {
	color: @buttonActiveColor;
	background-color: @buttonActiveBackgroundColor;
}

.button { 
	margin-right: 5px;
}

.dropdown { 
	display: inline;
}

.header { 
	.breadcrumb { 
		padding-bottom: 0px;		
	}
}

.scan { 
	textarea { 
		width: 100%;
		height: 200px;
	}
	
	#action-restart-spy { 
		float: right;
	}
}

.scan-result { 
	.system { 
		h3 { 
			display: inline;
			margin-right: 10px;
		}
		
		a {  
			margin-right: 3px;
			img {  
				margin-bottom: 8px;
			}
		}
	 }

	.permalink {
		margin-right: 15px;
	}

	.analysis { 
		.buttons { 
			text-align: center;
			
			a { 
				width: 60px;
			}
		}
		
		.movement-analysis { 
			table {	 
				tr { 
					td {
						&.c1 { 
							width: 180px;
						}

						&.c2 { 
							a {
								font-size: 0.9em;
								margin-right: 10px;
							}							
						}
					}
				}
			}
		}
	}	

	.totals { 
		.ship-class { 
			margin-left: 5px;
			margin-right: 5px;
		}

		.ships { 
			table {	 
				tr { 
					td {
						&.c1 { 
							width: 50px;
						}

						&.c2 { 
							width: 180px;
						}

						&.c3 { 
							width: 220px;
						}
					}
				}
			}
		}
		
		
		.characters { 					
			.alert {
				padding-right: 3px;
			}

			table { 
				tr {
					td {
						&.c1 {
							a {
								margin-top: 5px;
								margin-left: 3px;
							}							

							.dropdown { 
								margin-top: 5px;
							}
						}
					}
				}
			}
						
			.corporation { 
				padding: 3px;
				padding-bottom: 0px;
				
				strong {
					margin-left: 5px;
				}
				
				a {
					margin-left: 3px;
				}
			}
			
			.alliance {
				font-size: 0.8em;
				font-style: italic;
				padding-left: 3px;
			}
		}
	}	
}

.knowledge-base { 
	.badge { 
		margin-right: 5px;
	}
}
'
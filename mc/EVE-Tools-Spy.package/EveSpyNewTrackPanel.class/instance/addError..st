private
addError: aString
	| panel |
	
	self removeErrorsIfPresent.
	self addFirst: ((panel := REPanel new) 
		class: 'alert alert-error';
		add: (REAnchor new 
			class: 'close';
			label: 'x';
			callback: [ panel asClient fadeOut: 0.3 second ] asReefClientCallback;
			yourself);
		add: (REHtml with: '<strong>Error:&nbsp;</strong>');
		add: aString;
		yourself).
	self refresh.
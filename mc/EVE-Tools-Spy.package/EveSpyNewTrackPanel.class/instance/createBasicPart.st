factory
createBasicPart
	^(RESimpleSpan span: 12)
		add: (RETextField new
			example: 'System name';
			addDecoration: (REAutocompleteDecoration new 
				search: [ :v | self resolveSearch: v trimBoth ];
				yourself);
			disableEnter;
			on: #systemName of: self);
		yourself
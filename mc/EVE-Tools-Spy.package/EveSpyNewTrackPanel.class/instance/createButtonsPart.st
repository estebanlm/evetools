factory
createButtonsPart
	^REContainer new
		add: ((RESimpleSpan span: 8) 
			add: (REButton new
				class: 'btn btn-primary';
				label: 'Go!';
				callback: [ self executeAnalysis ] asReefTriggerCallback;
				yourself);
			add: ('Local scan can take some time (since it needs to refresh pilot status)... please be patient :)' asReefView 
				class: 'warning';
				yourself);
			yourself);
		add: ((RESimpleSpan span: 4)
			add: (REAnchor new 
				id: #'action-restart-spy';
				label: 'Restart Spy';
				callback: [ self restartSpy ] asReefTriggerCallback;
				yourself);
			yourself);
		yourself
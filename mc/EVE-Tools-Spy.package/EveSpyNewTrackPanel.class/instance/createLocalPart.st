factory
createLocalPart
	^REPanel new 
		add: ('Local paste' asReefHeading level: 4);
		add: ( RETextArea new
			example: self localExampleText;
			callback: [ :v | self localText: v ];
			yourself);
		yourself
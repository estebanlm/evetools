factory
createPastePart
	^REContainer new 
		add: ((RESimpleSpan span: 6)
			add: self createScanPart;
			yourself);
		add: ((RESimpleSpan span: 6)
			add: self createLocalPart;
			yourself);
		yourself

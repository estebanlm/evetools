factory
createScanPart
	^REPanel new 
		add: ('Scan paste' asReefHeading level: 4);
		add: (RETextArea new
			example: self scanExampleText;
			callback: [ :v | self scanText: v ];
			yourself);
		yourself
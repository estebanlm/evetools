factory
createTrack
	^ EveTrack 
		fromSystem: self systemName
		scan: self scanText 
		local: self localText.

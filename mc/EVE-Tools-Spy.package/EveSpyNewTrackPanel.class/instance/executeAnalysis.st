actions
executeAnalysis
	| track |

	[ track := self createTrack.
	  track isEmpty 
		ifTrue: [ self addError: 'Your scan does not has any ships or pilots to track.' ]
	  	ifFalse: [
			track save. 
			self spy addTrack: track.
			self announce: (EveTrackAdded track: track).
			self show: (EveSpyTrackResultPanel for: track) ] ]
	on: Error do: [ :e |
		self addError: 'The text contains errors and cannot be parsed. Please verify your pastes and try again.' ].
initialization
initializeContents 
	self class: 'scan'.

	self add: (self title asReefHeading level: 3).
	self add: (REForm new
		add: (RESimpleRow with: self createBasicPart);
		add: (RESimpleRow with: self createPastePart);
		add: (RESimpleRow with: self createButtonsPart);
		yourself)
private
removeErrorsIfPresent
	(self children 
		select: [ :each | each classes includesSubstring: 'alert-error' ])
		do: [ :each | self remove: each ]
private
resolveSearch: aString
 	^aString size > 1
		ifTrue: [ EveSystem findByNameMatching: aString limit: EveSpyApplication searchLimit ]
		ifFalse: [ #() ]
paths
about
	<get>
	<path: '/about'>

	self continueWithRoot: [ :root | 
		root startOn: EveSpyAbout new ].

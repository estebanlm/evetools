paths
pilot: id
	<get>
	<path: '/pilot/{id}'>
	
	| pilot |
	
	pilot := EveCharacter 
		findById: id asNumber
		ifAbsent: [ ^ self notFound  ].
		
	self continueWithRoot: [ :root | 
		root startOn: (EveSpyCharacterPanel for: pilot) ].
paths
system: id
	<get>
	<path: '/system/{id}'>
	
	| system |
	
	system := EveSystem 
		findById: id asNumber
		ifAbsent: [ ^ self notFound  ].
		
	self continueWithRoot: [ :root | 
		root startOn: (EveSpySystemPanel for: system) ].
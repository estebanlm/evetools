paths
track: id
	<get>
	<path: '/track/{id}'>
	
	| track |
	
	track := EveTrack 
		findById: id
		ifAbsent: [ ^ self notFound  ].
		
	self continueWithRoot: [ :root | 
		root startOn: (EveSpyTrackResultPanel for: track) ].
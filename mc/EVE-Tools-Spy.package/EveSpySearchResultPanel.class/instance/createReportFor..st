private
createReportFor: aCollection	
	^ (RESimpleSpan span: 6)
		add: (self createReportGridFor: aCollection);
		yourself.
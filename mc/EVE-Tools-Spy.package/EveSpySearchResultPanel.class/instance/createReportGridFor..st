private
createReportGridFor: aCollection
	| grid |
	
	grid := RESimpleGrid new
		columns: 2;
		yourself.
	
	aCollection do: [ :eachTrack | 
		grid 
			add: eachTrack date asString; 
			add: (REAnchor new 
				label: 'See';
				url: (self urlFor: eachTrack);
				yourself) ].

	^ grid
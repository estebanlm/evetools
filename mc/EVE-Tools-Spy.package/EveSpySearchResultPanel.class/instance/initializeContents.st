initialization
initializeContents
	self class: 'row'.
	self class: 'system'.
	
	self add: (REHeading new 
		text: self createHeader;
		level: 3;
		yourself).
	self add: (REPanel new 
		add: (('Latest ', self limit asString, ' visits') asReefHeading level: 5);
		yourself).
	
	self add: (self shownTracks
		ifNotEmpty: [ :shown | self createReportFor: shown ]
		ifEmpty: [ '(No scans so far)' ])

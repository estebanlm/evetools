private
addPanels
	(self spy isShowingAnalysis: EveSpyShowingAnalysis movement)
		ifTrue: [ ^ self addMovementPanel ].
	(self spy isShowingAnalysis: EveSpyShowingAnalysis both)
		ifTrue: [ ^ self addBothPanels ].
	(self spy isShowingAnalysis: EveSpyShowingAnalysis static)
		ifTrue: [ ^ self addStaticPanel ]
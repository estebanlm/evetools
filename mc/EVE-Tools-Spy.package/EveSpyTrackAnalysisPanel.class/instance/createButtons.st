private
createButtons
	^ REPanel new 
		class: 'btn-group';
		attributeAt: 'data-toggle' put: 'buttons-radio'; 
		add: (REAnchor new 
			class: 'btn btn-mini';
			class: 'active' if: (self spy isShowingAnalysis: EveSpyShowingAnalysis movement);
			label: 'Movement';
			callback: [ self showMovement ];
			yourself);
		add: (REAnchor new 
			class: 'btn btn-mini';
			class: 'active' if: (self spy isShowingAnalysis: EveSpyShowingAnalysis both);
			label: 'All';
			callback: [ self showBoth ];
			yourself);
		add: (REAnchor new 
			class: 'btn btn-mini';
			class: 'active' if: (self spy isShowingAnalysis: EveSpyShowingAnalysis static);
			label: 'Static';
			callback: [ self showStatic ];
			yourself);
		yourself.
		
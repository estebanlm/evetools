private
createCharacterMovementPanel
	^self 
		createMovementAnalysis: self analysis charactersMoving 
		ifEmpty: [ '(No pilots matching)' ].

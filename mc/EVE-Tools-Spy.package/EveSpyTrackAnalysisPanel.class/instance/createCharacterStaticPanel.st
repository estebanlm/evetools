private
createCharacterStaticPanel
	^ self 
		createStaticAnalysis: self analysis charactersStaticNew	
		ifEmpty: [ '(No new pilots)' ]
private
createMovementAnalysis: repetition ifEmpty: emptyBlock
	| grid |
	
	repetition ifEmpty: [ ^ emptyBlock value ].
	
	grid := RESimpleGrid new
		columns: 2;
		yourself.
		
	(repetition associations 
		sorted: [ :a :b | a value size > b value size ])
		do: [ :each | 
			grid 
				add: each key;
				add: ((each value 
					sorted: [ :a :b | a date > b date ])
					collect: [ :eachTrack |
						REAnchor new 
							label: eachTrack systemName;
							callback: [ self showTrack: eachTrack ] ]) ].
			
	^grid
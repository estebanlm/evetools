private
createMovementPanel
	^REPanel new
		class: 'movement-analysis';
		add: ('Moving pilots' asReefHeading level: 4);
		add: self createCharacterMovementPanel;
		add: ('Moving ships' asReefHeading level: 4);
		add: self createShipsMovementPanel;
		yourself
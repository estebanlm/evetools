private
createShipStaticPanel
	^ self 
		createStaticAnalysis: self analysis shipsStaticNew
		ifEmpty: [ '(No new ships)' ]
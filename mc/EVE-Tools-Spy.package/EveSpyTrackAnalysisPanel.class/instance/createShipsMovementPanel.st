private
createShipsMovementPanel
	^self 
		createMovementAnalysis: self analysis shipsMoving
		ifEmpty: [ '(No ships matching)' ].
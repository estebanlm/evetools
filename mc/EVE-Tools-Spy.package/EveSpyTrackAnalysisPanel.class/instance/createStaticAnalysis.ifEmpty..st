private
createStaticAnalysis: aCollection ifEmpty: emptyBlock
	
	aCollection ifEmpty: [ ^ emptyBlock value ].
	
	^ RESimpleGrid new 
		columns: 1;
		addAll: aCollection;
		yourself
	
private
createStaticPanel
	^REPanel new
		class: 'static-analysis';
		add: ('New pilots' asReefHeading level: 4);
		add: self createCharacterStaticPanel;
		add: ('New ships' asReefHeading level: 4);
		add: self createShipStaticPanel;
		yourself
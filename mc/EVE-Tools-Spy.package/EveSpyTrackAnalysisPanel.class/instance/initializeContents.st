initialization
initializeContents
	self class: 'analysis'.

	self add: (analysisPanel := RESimpleRow new).
	self addPanels.
	self add: (RESimpleRow new
		class: 'buttons';
		add: ((RESimpleSpan span: 12)
			add: self createButtons;
			yourself);
		yourself).
actions
showBoth
	self spy showingAnalysis: EveSpyShowingAnalysis both.
	analysisPanel removeAll. 
	self addBothPanels.
	analysisPanel refresh.
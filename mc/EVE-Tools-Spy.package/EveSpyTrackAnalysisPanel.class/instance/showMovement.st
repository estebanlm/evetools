actions
showMovement
	self spy showingAnalysis: EveSpyShowingAnalysis movement.
	analysisPanel removeAll. 
	self addMovementPanel.
	analysisPanel refresh.
actions
showStatic
	self spy showingAnalysis: EveSpyShowingAnalysis static.
	analysisPanel removeAll. 
	self addStaticPanel.
	analysisPanel refresh.
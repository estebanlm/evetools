private
createCharacter: aCharacter
	^ REContainer new 
		add: (REImage url: aCharacter urlPortrait30);
		add: (aCharacter name asReefText
			addDecoration: (RESimplePopoverDecoration new 
				placement: 'left';
				title: (REPanel new 
					add: aCharacter name;
					add: ((REImage url: aCharacter urlPortrait64)
						class: 'pull-right';
						yourself);
					yourself);
				tooltip: (self createTooltipFor: aCharacter);
				yourself);
			yourself);
		add: ((self createMuteButtonFor: aCharacter)
			class: 'pull-right';
			yourself);
		add: ((self createInfoDropdownFor: aCharacter)
			class: 'pull-right';
			yourself);
		yourself
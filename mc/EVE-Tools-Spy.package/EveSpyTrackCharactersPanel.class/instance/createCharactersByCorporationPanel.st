private
createCharactersByCorporationPanel
	| panel |
		
	panel := REContainer new.
			
	self local
		ifNotEmpty: [
			(self local corporationsAndCharacters associations
				sorted: [ :a :b | a key name < b key name ]) 
				do: [ :corpAndChars | 
					panel addAll: (self 
						createCorporation: corpAndChars key 
						characters: (corpAndChars value sorted: [ :a :b | a name < b name ])) ] ]
		ifEmpty: [ 
			panel add: '(No local results)' ].

	^panel
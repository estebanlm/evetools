private
createCharactersByNamePanel
	| panel |
		
	panel := REContainer new.
			
	self local
		ifNotEmpty: [
			|  grid |
			
			grid := RESimpleGrid new
				columns: 1;
				yourself.
			(self local characters
				sorted: [ :a :b | a name < b name ]) 
				do: [ :character | grid add: (self createCharacter: character) ].
			
			panel add: grid. ]
		ifEmpty: [ 
			panel add: '(No local results)' ].

	^panel
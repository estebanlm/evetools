private
createCharactersPanel
	^ REPanel with: (sortByCorp 
		ifTrue: [ self createCharactersByCorporationPanel ]
		ifFalse: [ self createCharactersByNamePanel ])
private
createCorporation: corporation  characters: characters
	| container grid |
	
	grid := RESimpleGrid new
		columns: 1;
		yourself.
						
	grid addAll: (characters 
		collect: [ :each | self createCharacter: each ]).
			
	(container := REPanel new)
		add: (REPanel new 
			class: 'alert';
			add: (REPanel new 
				class: 'corporation';
				add: (REPanel new 
					class: 'badge badge-inverse';
					add: characters size;
					yourself); 
				add: corporation name asReefStrong;
				"add: (REAnchor new 
					label: corporation name asReefStrong;
					callback: [ self showCorporation: corporation ];
					yourself);"
				add: ((self createMuteButtonFor: corporation)	
					class: 'pull-right';
					yourself);
				add: ((self createInfoDropdownFor: corporation)
					class: 'pull-right';
					yourself);
				yourself);
			add: (characters anyOne alliance name asReefText
				class: 'alliance';
				yourself);
			yourself);
		add: grid.

	^ container
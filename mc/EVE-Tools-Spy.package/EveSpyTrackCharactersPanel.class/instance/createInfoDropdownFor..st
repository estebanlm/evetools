private
createInfoDropdownFor: anObject 
	^ RESimpleDropdown new
		caret: false;
		title: '[ + ]';
		add: (REAnchor new 
			label: 'EVE-Who';
			url: anObject urlEVEWho;
			yourself);
		add: (REAnchor new 
			label: 'Battleclinic';
			url: anObject urlBattleclinic;
			yourself);
		add: (REAnchor new 
			label: 'zKillboard';
			url: anObject urlZKillboard;
			yourself);
		yourself
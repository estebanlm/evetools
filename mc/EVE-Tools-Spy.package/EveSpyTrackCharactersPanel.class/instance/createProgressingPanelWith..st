private
createProgressingPanelWith: aBlock 
	^ REProgressingPanel new
		message: ('Updating {1} pilots...' format: { self updateTask total });
		timeout: 1 second;
		total: self updateTask total;
		advance: [ self updateTask updated ];
		completeWhen: [ self updateTask isFinish ];
		failWhen: [ self updateTask isError ];
		failContent: [ 
			| lastError |
			lastError := self updateTask lastError.
			lastError isEveError 
				ifTrue: [ 'Error: ', lastError messageText ]
				ifFalse: [ 'Error while processing, try again!' ] ];
		contents: aBlock
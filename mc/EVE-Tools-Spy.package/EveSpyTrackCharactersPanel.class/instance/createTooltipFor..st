private
createTooltipFor: aCharacter 
	^ REGrid new 
		class: 'table';
		columns: 2;
		add: 'Security status';
		add: aCharacter roundedSecurityStatus;
		add: 'Age';
		add: aCharacter ageAsString;
		add: 'Corp';
		add: aCharacter corporationName;
		add: 'Time in corp.';
		add: aCharacter timeInCorpAsString;
		yourself
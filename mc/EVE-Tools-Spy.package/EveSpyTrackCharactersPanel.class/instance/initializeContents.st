initialization
initializeContents 
	self class: 'characters'.
	
	self add: (REHeading new
		level: 4;
		text: (
			'Pilots' asReefView", 
			(REAnchor new
				class: 'btn pull-right';
				class: 'active' if: sortByCorp;
				attributeAt: 'data-toggle' put: 'button';
				label: 'Sort by Corporation';
				callback: [ self toggleSort ];
				yourself)");
		yourself).
	
	charactersPanel := self add: ((self updateTask total > 0) 
		ifTrue: [ self createProgressingPanelWith:  [ self createCharactersPanel ] ]
		ifFalse: [ self createCharactersPanel ]).
actions
toggleSort
	sortByCorp := sortByCorp not.
	sortByCorp ifTrue: [ self local updateCharacters ].
	charactersPanel removeAll.
	charactersPanel add: (sortByCorp 
		ifTrue: [ self createCharactersByCorporationPanel ]
		ifFalse: [ self createCharactersByNamePanel ]).
	charactersPanel refresh.
private
classForTeckLevel: level
	level = EveTechLevel t1 ifTrue: [ ^'label-info' ].
	level = EveTechLevel t2 ifTrue: [ ^'label-warning' ].
	level = EveTechLevel faction ifTrue: [ ^'label-success' ].
	level = EveTechLevel t3 ifTrue: [ ^'label-important' ].
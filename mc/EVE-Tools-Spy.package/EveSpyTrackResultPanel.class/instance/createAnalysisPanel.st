private
createAnalysisPanel
	^ (EveSpyTrackAnalysisPanel for: self track)
		on: EveAnnouncement do: [ :ann | self announce: ann ];
		yourself
private
createCharactersPanel
	^  (EveSpyTrackCharactersPanel for: self track)
			class: 'span4';
			on: EveAnnouncement do: [ :ann | self announce: ann ];
			yourself
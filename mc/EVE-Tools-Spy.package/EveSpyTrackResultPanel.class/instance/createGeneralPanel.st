private
createGeneralPanel
	^RESimpleRow new
		add: ((RESimpleSpan span: 6)
			add: self createSystemPart;
			add: 'Visited on ', self track date asString;
			yourself);
		"add: ((RESimpleSpan span: 6)
			add: self createPermalinkPart;
			yourself);"
		yourself
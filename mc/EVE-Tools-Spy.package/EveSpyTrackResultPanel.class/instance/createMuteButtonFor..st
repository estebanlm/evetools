private
createMuteButtonFor: anObject 
	^ REAnchor new 
		class: 'btn btn-mini';
		class: 'active' if: (self spy isMuted: anObject);
		attributeAt: 'data-toggle' put: 'button';
		callback: [ self mute: anObject ];
		label: 'l l';
		yourself
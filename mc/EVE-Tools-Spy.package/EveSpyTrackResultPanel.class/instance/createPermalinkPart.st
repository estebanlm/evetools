private
createPermalinkPart
	| anchor url |
	
	url := EveApplication urlApplication
		addPathSegments: self path;
		asString.
	
	(anchor := REAnchor new) 
		class: 'permalink';
		label: url;
		callback: [ self refresh ];
		yourself.
	
	^REContainer new
		add: ('Permalink: ' asReefLabelFor: anchor);
		add: anchor;
		add: (REAnchor new 
			class: 'btn btn-mini';
			label: 'Copy';
			callback: [ :me |
				me asClient 
					call: 'copyToClipboard' with: url ] asReefClientCallback;
			yourself);
		yourself.
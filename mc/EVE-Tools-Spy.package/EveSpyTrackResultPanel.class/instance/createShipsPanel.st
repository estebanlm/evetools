private
createShipsPanel
	| panel |
	
	panel := REPanel new
		class: 'span8';
		class: 'ships';
		add: ('Ships' asReefHeading level: 4);
		yourself.
	
	self scan
		ifNotEmpty: [  
			(self scan shipClassesAndObjects associations 
				sorted: [ :a :b | a key name < b key name ])
				do: [ :eachAssoc |
					panel addAll: (self shipClassGroup: eachAssoc key ships: eachAssoc value) ] ]
		ifEmpty: [ 
			panel add: '(No scan results)' ].
	
	^panel
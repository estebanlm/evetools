private
createSystemPart
	| panel |
	
	panel := REPanel new 
		class: 'system';
		add: (self track systemName asReefHeading level: 3);
		yourself.

	self track isSystemKnown
		ifTrue: [
			panel
				"add: (REAnchor new 
					label: (REImage url: EveLibrary / #viewPng);
					callback: [ self showSystem ];
					yourself);"
				add: (REAnchor new 
					label: (REImage url: EveLibrary / #dotlanPng);
					url: self track systemUrlDotlan;
					yourself) ].
		
	^ panel

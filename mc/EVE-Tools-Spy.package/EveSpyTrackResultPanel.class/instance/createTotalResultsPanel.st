private
createTotalResultsPanel
	^RESimpleRow new
		class: 'totals';
		add: self createShipsPanel;
		add: self createCharactersPanel;
		yourself
private
createTrackPanel
	^(EveSpyUpdateTrackPanel for: self track)
		on: EveAnnouncement do: [ :ann | self announce: ann ];
		yourself
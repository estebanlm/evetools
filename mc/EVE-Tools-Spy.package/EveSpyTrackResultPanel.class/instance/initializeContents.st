initialization
initializeContents	
	self class: 'scan-result'.

	self add: self createGeneralPanel.
	self add: (REPanel new class: 'page-header').
	self add: [ 
		REContainer new
			add: self createAnalysisPanel;
			add: (REPanel new class: 'page-header') ] if: self isTrackInSpy.
	self add: self createTotalResultsPanel.
	self add: (REPanel new class: 'page-header').
	self add: self createTrackPanel.
private
shipClassGroup: shipClass ships: ships
	| panel grid |
	
	panel := REPanel new.
	
	"Header"
	panel add: (REPanel new
		class: 'alert';
		add:(REPanel new
			class: 'badge badge-inverse';
			add: ships size;
			yourself);
		add: (REText new 
			class: 'ship-class';
			text: shipClass name;
			yourself);
		yourself).
	
	"Ships"
	grid := RESimpleGrid new
		columns: 3;
		yourself.
		
	(ships 
		sorted: [ :a :b | a type name < b type name ])
		do: [ :each | 
			grid 
				add: (REPanel new 
					class: 'label';
					class: (self classForTeckLevel: each type techLevel);
					add: each type techLevel;
					yourself);
				add: each type;
				add: each name
				"There is not way to know if a ship with same name and same class is actually the same ship"
				";
				add: ((self createMuteButtonFor: each)
					class: 'pull-right';
					yourself)" ].

	panel add: grid.
	
	^panel
	

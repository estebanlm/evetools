accessing
allTracks
	^self spy tracks sorted: [ :a :b | a date > b date ]
private
createTrackHistory 
	| grid |
	
	grid := RESimpleGrid new
		columns: 3;
		yourself.
		
	self allTracks do: [ :track |
		grid 
			add: track date;
			add: track systemName;
			add: (REAnchor new 
				label: 'See';
				url: (self urlFor: track);
				yourself);
			yourself ].
		
	^ (REPanel new 
		class: 'span6';
		add: grid;
		yourself).
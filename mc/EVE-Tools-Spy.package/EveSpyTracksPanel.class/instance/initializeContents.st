initialization
initializeContents 
	self class: 'row'.
	self class: 'tracks'.
	
	self add: ('Track history' asReefHeading level: 3).	
	self add: (self spy isEmpty 
		ifTrue: [ '(No tracks to show now)' ]
		ifFalse: [ self createTrackHistory ] ) 
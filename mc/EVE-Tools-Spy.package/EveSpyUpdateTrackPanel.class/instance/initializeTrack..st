initialization
initializeTrack: aTrack
	track := aTrack.
	self systemName: (track isSystemUnknown
		ifTrue: [ '' ]
		ifFalse: [ aTrack systemName ] ).
	self initialize.
	
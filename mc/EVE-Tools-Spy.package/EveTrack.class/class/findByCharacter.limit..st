accessing
findByCharacter: aCharacter limit: aNumber
	| selected id |
	
	id := self voyageRepository keyOf: aCharacter.
	selected := (self selectMany: [ :each | (each at: 'local.characters.__id') = id ] asMongoQuery)	
		sorted: [ :a :b | a date > b date ].
	^ selected  size > aNumber 
		ifTrue: [ selected first: aNumber ]
		ifFalse: [ selected ]
accessing
findBySystem: aSystem limit: aNumber
	| id |
	
	id := self voyageRepository keyOf: aSystem.
	^ self 
		selectMany: [ :each | (each at: 'system.__id') = id ] asMongoQuery
		sortBy: { #date -> -1 } asDictionary
		limit: aNumber
		offset: 0
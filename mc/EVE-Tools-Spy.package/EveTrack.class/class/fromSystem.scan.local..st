instance creation
fromSystem: systemString scan: scanString local: localString
	^self basicNew 
		initializeSystem: systemString 
			scan: scanString 
			local: localString;
		yourself 
private
createUniqueId
	^ self class voyageRepository nextTrackId.  
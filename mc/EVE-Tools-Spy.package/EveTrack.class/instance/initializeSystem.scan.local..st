initialization
initializeSystem: systemString scan: scanString local: localString
	self initialize.		
	
	system := systemString trimBoth 
		ifNotEmpty: [
			EveSystem 
				findByExactName: systemString 
				ifAbsent: [ (EveSystem name: systemString) save ] ]
		ifEmpty: [ 
			EveSystem unknown ].
	
	scan := EveScan fromString: scanString.
	local := EveLocal fromString: localString.
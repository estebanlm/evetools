stressing
runStress1
	| lists sem |
	
	lists := (1 to: 10) collect: [ :i | self perform: ('corp', i asString) asSymbol ].
	
	sem := Semaphore forMutualExclusion.
	lists do: [ :each |
		[ (EveLocal fromString: each) updateTask updateCharacters.
		  sem signal. ] fork.
		sem wait.  ]

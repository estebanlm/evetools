tests
testNextId 
	| nextId |
	
	nextId := self generator nextId.	
	self assert: nextId size equals: 12.
tests
testNextIdMany
	| nextIds |
	
	nextIds := Set new.
	10000 timesRepeat: [  
		nextIds add: self generator nextId ].	
	self assert: nextIds size equals: 10000.
register
registerApplicationAt: aString repository: aRepository
	| app |
	
	self = EveApplication 
		ifTrue: [ self error: 'I am abstract. Please register one of my children instead' ].
		
	app := REApplication 
		registerAsApplication: aString 
		root: self
		theme: JQVaderTheme.
	
	app configuration addParent: VORepositoryConfiguration instance.
	app removeLibrary: REBootstrapLibrary.			
	app addFilter: VORepositoryFilter new.
	app addFilter: self filterClass new.	
	app 
		preferenceAt: #voyageRepository put: aRepository;
		preferenceAt: #trackingStrategy put: WACookieIfSupportedSessionTrackingStrategy new.

	^app
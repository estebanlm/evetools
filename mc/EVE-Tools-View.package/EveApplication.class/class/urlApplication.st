attributes
urlApplication
	| requestContext  |
	
	requestContext := WACurrentRequestContext value.
	^ self urlBase
		addPathSegments: (requestContext application urlFor: requestContext session) path;
		yourself.
attributes
urlBase
	| requestContext request |
	
	requestContext := WACurrentRequestContext value.
	request := requestContext request.
	
	^ ZnUrl new
		scheme: #http;
		host: (request host copyUpTo: $:);
		port: request port;
		yourself.
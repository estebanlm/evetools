accessing
header
	^ header ifNil: [ 	
			header := self createHeaderPanel
				subscribeToShowOn: self;
				yourself ]
initialization
initialize 
	super initialize.
	self addDecoration: EveWarningBackButton new.
	self addDecoration: EveNonExpireDecoration new.

	self startOn: self createDefaultPanel.
rendering
renderContentOn: html
	html div 
		class: 'container';
		with: [
			html render: self header. 
			html render: self content ].
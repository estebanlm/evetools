not used
respondRendered
	| application session |
	
	application := self requestContext application.
	session := self requestContext session.
	self requestContext respond: [:response |
		response
			contentType: WAMimeType textHtml;
			nextPutAll: (	WAHtmlCanvas builder
				fullDocument: true;
				rootBlock: [ :root | 
					root meta contentType: application contentType.
					root meta contentScriptType: WAMimeType textJavascript.
					root htmlAttributes
						at: 'xmlns' put: 'http://www.w3.org/1999/xhtml';
						at: 'xml:lang' put: 'en';
						at: 'lang' put: 'en'.
					root beXhtml10Strict.
					application libraries do: [ :each | each default updateRoot: root ].
					session updateRoot: root.
					self updateRoot: root ];
				render: [ :canvas | self renderOn: canvas ]) ].
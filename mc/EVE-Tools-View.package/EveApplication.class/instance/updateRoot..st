updating
updateRoot: anHtmlRoot
	super updateRoot: anHtmlRoot.
	
	anHtmlRoot title: self class applicationTitle.
	anHtmlRoot javascript url: REBootstrapLibrary / #bootstrapminJs.
	anHtmlRoot stylesheet url: REBootstrapLibrary / #styleCss.		
	anHtmlRoot stylesheet url: EveLibrary / #bootstrapminCss.
	anHtmlRoot javascript url: EveLibrary / #copyClipboardJs.
	anHtmlRoot javascript url: JQDeploymentLibrary / #jqueryalphanumericpackJs.

	anHtmlRoot link
		relationship: 'stylesheet/less';
		url: self urlStyle.
	anHtmlRoot javascript url: REBootstrapLibrary / #less130minJs.

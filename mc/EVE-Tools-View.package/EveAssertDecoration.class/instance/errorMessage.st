accessing
errorMessage
	^ errorMessage ifNil: [ self class defaultMessage ]
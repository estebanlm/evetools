rendering
renderContentOn: html
	self validate 
		ifTrue: [ self renderNextOn: html ]
		ifFalse: [ self renderErrorOn: html ]
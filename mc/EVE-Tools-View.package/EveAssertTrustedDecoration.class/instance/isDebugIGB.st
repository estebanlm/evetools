testing
isDebugIGB
	^ (self requestContext request 
		at:'DEBUGIGB'
		ifAbsent: [ '0' ]) = '1'
		
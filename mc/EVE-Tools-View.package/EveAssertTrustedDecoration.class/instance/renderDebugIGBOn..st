rendering
renderDebugIGBOn: html
	| grid |
	
	grid := RESimpleGrid new
		columns: 2;
		yourself. 
	self requestHeaders keysAndValuesDo: [ :key :value |
		grid 
			add: key asString;
			add: value asString ].
	
	html render: grid.
instance creation
on: aPart
	"Initializes with a starting part"
	^ self basicNew
		initialize: aPart;
		yourself
showing
doShow: aPart
	((aPart hasPath) and: [ REContext isInContext ])
		ifTrue: [ self basicShowWithPath: aPart ]
		ifFalse: [ self basicShow: aPart ].
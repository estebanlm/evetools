instance creation
with: aStringOrView
	^ self basicNew 
		initialize: aStringOrView;
		yourself
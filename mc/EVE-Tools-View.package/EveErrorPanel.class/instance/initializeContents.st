initialization
initializeContents 
	self class: 'container'.
	self class: 'general-error'.
	
	self add: (RESimpleRow with: (REPanel new 
		class: 'alert alert-error';
		add: 'Error: ' asReefStrong;
		add: self text;
		yourself))
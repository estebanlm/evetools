accessing
mimetypeOf: aSelector
	(aSelector endsWith: #Less) 
		ifTrue: [ ^ 'text/less' ].
	^ super mimetypeOf: aSelector.
applying
script
	^self view asClient 
		attributeAt: 'data-load' put: (self canvas urlForAction: [ 
			self requestContext respond: [ :response | 
				response 
					document: self tooltipAsString 
					mimeType: WAMimeType textHtml ] ]);
		call: 'popover' 
		with: { 
			#html -> true.
			#placement -> self placement.
			#trigger -> 'hover'.
			#title -> self titleAsString.
			#content -> (self canvas javascript 
				alert: (self canvas jQuery new alias: 'e');
				asFunction: #()).
		 } asDictionary
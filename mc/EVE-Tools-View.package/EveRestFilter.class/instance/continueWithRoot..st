private
continueWithRoot: aBlock
	"Continues the execution of the application. 
	 I will pass aBlock to the root class of app, which I assume is a REApplication. 
	 Otherwise, this will fail"
	| requestContext application session rootComponent |
	
	requestContext := self requestContext.	
	application := requestContext application.
	session := self obtainSessionWithApplication: application context: requestContext.

	requestContext 
		push: session 
		during: [
			rootComponent := requestContext rootComponentIfAbsent: [ nil ].
			rootComponent ifNil: [ 
				rootComponent := (application preferenceAt: #rootClass) new.
				requestContext rootComponent: rootComponent ].
			rootComponent do: aBlock.
			self next handleFiltered: requestContext ].
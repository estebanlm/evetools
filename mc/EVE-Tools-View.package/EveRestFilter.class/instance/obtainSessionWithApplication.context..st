private
obtainSessionWithApplication: application context: requestContext
	| sessionKey session |
	
	sessionKey  := application trackingStrategy keyFromContext: requestContext.	
	session := sessionKey ifNotNil: [  
		application cache 
			at: sessionKey  
			ifAbsent: [ nil] ].
	
	session  ifNil: [ 
		session := application newSession.
		application register: session ].
		
	^ session
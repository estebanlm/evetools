as yet unclassified
confirmMessage
	^String streamContents: [ :stream |
		stream 
			<< 'You are going to leave '
			<< EveSpyApplication applicationTitle << '!' << String cr 
			<< 'All your tracks will be lost.' ]
*EVE-Tools-View
basePathWith: aString
	^ (self baseUrl 
		addToPath: aString)
		asString
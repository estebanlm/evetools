*EVE-Tools-View
basePathWithAll: aCollection
	^ (self baseUrl 
		addAllToPath: aCollection)
		asString
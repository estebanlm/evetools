*EVE-Tools-View
port 
	| port |
	port := self host copyAfterLast: $:.
	^(port notEmpty and: [ port ~= '90' ])
		ifTrue: [ port asInteger ]
		ifFalse: [ nil ] 